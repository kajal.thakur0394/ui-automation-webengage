
# Dashboard Automation

Test Automation framework built to test out the WebEngage Dashboard. The framework comprises multiple test suites, each configurable to test out the product on various aspects.


## Requirements
In order to setup this project you'll need following things installed and configured on your local machine
- Maven
- Java
- Browsers
    - Chrome
    - Firefox
- Java IDE
    - Cucumber Plugin
    - JSON Editor Plugin
    - Jenkins Editor Plugin
- Microsoft Excel

You'll need Office365 account if you want to add/update API endpoints or payloads.

## Tech Stack

- Cucumber
- Serenity
- Selenium WebDriver
- Rest Assured
- JSch
- JUnit
- Apache POI
- JSON-simple
- Log4j
- Apache HTTP Server


## Test Suites
Tests have been categorized into various suites to give user flexibility to choose test type. Also, will help in framework maintenance and test readability.
Goals and capabilities of each suite has been described below

### Smoke Test Suite

Smoke tests get executed on the test account and can also be executed on a specific account. These tests are tagged at scenario level. The tests having tags `@skip` and `@disabled` get excluded from execution.
They essentially validate all the UI and API related stuff.

Visit following [link](https://www.confluence.com "Smoke Test Scenarios") to get a list of all the Smoke scenarios.

### Regression Test Suite

Regression tests get executed on the test account and can also be executed on a specific account. These tags are tagged at feature level. The tests having following tags along with `@Regression` tags get excluded from execution.
They essentially validate all the UI and API related stuff.
- `@skip`
- `@disabled`
- `@End-to-End`
- `@SDKTesting`

Visit following [link](https://www.confluence.com "Regression Test Scenarios") to get a list of all the Regression scenarios.

### End-To-End Suite

End-To-End tests get executed on the test account. These tests are tagged at scenario level. The tests having tags `@skip` and `@disabled` get excluded from execution.
These tests have been designed to validate end user delivery by making use of valid integration creds.

Visit following [link](https://www.confluence.com "End-To-End Test Scenarios") to get a list of all the End-To-End scenarios.

### SDKTesting Test Suite

SDKTesting tests get executed on the test account. These tests are tagged at scenario level. The tests having tags `@skip` and `@disabled` get excluded from execution.
These tests validate the WebSDK and MobileSDKs on multiple fronts. 

Visit following [link](https://www.confluence.com "Smoke Test Scenarios") to get a list of all the Smoke scenarios.

### Test Data Creation Suite
Test Data that is being used by dedicated automation test accounts can be injected into existing accounts or also a new account with this test data can be instated. Execution is driven by API.

[Take a glance at the Test Data](https://www.confluence.com "Dashboard Pre-requisite Test Data")

### SSP Onboarding
A helper automation suite that'll help to quickly test out Regression and End-to-End scenarios for newly onboarded SMS Service Provider.
Input JSON Templates need to be placed in SFTP root location. Please refer this [doc](https://www.confluence.com "Helper Task - SSP Onboarding") to make use of this suite.
### Campaign Stats
Custom helper suite that has been built for populating stats of desired campaign and then metrics processed, dumped and provided as CSVs via Slack reporting for further analysis or Testing.
Input JSON Templates need to be placed in SFTP root location. Please refer this [doc](https://www.confluence.com "Helper Task - Campaign Stats") to make use of this suite.
## Capabilities

### Modules
The tests have been grouped further on the basis of dashboard sections they address. This has been termed as modules in the framework. You can select modules that you wish to include/exclude from suite execution cycle.
Modules enabled till date
- Users
- Analytics
- LiveSegments
- StaticSegments
- Integration
- SMSCampaigns
- EmailCampaigns
- PushCampaigns
- Journeys
- OnsiteNotification
- InLineContentWebP
- InLineContentAppP
- OnsiteSurveys
- InAppCampaigns
- Whatsapp
- WebPushCampaign

### Environments
The above suites (excluding CampaignStats) can be executed on any of the environments
- Prod US
- Prod IN
- Prod UNL
- Prod Beta
- Staging
    - Default Namespace
    - `automation` Namespace
    - Custom Namespace (User needs to define namespace in prompt)
- QA (Configured but under test)

### Platforms & Browsers
To check cross browser compatibility the tests can be executed on Chrome, Firefox and Safari. And, you can also direct this execution either on Windows or MacOS.

## Usage

You can debug this project as JUnit Test by pointing to TestRunner.java.
But, this won't generate Test Reports and will also bypass Test Initalizer configuration.

To achieve the above you need to execute this as a maven project
```bash
  mvn clean compiler:compile
```

```bash
  mvn exec:java -Dset.Browser=chrome-headless -Dset.Environment="Prod US" -Dset.CredentialsUN=aditya.dhanve@webengage.com -Dset.CredentialsPW=**** -Dset.Suite="Regression" -Dset.Module="All" -Dset.Namespace=automation -Dset.LicenseCode="null" -Dset.BuildNumber="null" verify 
```
## Execution

Team has configured CICD pipeline to execute builds daily on dedicated Windows Machine. 
The schedule of builds are as following

|    Suite   	| Environment 	| Day of Week 	|   Time  	|
|:----------:	|:-----------:	|:-----------:	|:-------:	|
| Regression 	| Prod US     	| Everyday    	| 0130hrs 	|
| Regression 	| Prod IN     	| Everyday    	| 0500hrs 	|
| Regression 	| Staging     	| Wednesday   	| 0200hrs 	|
| End-To-End 	| Prod US     	| Everyday    	| 0430hrs 	|
| End-To-End 	| Prod IN     	| Everyday    	| 0505hrs 	|
| SDKTesting 	| Prod US     	| Everyday    	| 0730hrs 	|
| SDKTesting 	| Prod IN     	| Everyday    	| 0510hrs 	|

User can also trigger build by passing parameter directly via Jenkins. User will be given an option to choose the machine to execute the suite on (Jenkins node), the default selection is Windows instance.

[Click here](https://jenkins.stg.webengage.biz/job/Dashboard%20Automation/build?delay=0sec "Trigger Dashboard Automation") to Trigger the Pipeline


## Reporting

The builds that get triggered or are explicitly triggered by the User get reported to [#testautomation](https://webklipper.slack.com/archives/C02AZHRRBLJ "Alerts channel") channel with all metadata of the job and have the report links and build details embedded.
Depending on Suite that is executed the Slack thread varies. For certain suites, you'll see more results in the thread.
The report are descriptive and are in business language, screenshots get attached in case of failures, have detailed API request and response logs and for issues already existing on Prod there are JIRA issues linked. 
## Documentation

Refer to following docs to get more info about the project

[Architecture](https://webengage.atlassian.net/wiki/spaces/QA/pages/3040215041/UI+Automation+v1.3+March+2022)

[Latest Release Notes](https://repo.webengage.com/qa/ui-automation/-/releases)

[Releases](https://linktodocumentation)

[Jenkins Pipeline](https://jenkins.stg.webengage.biz/job/Dashboard%20Automation/)

