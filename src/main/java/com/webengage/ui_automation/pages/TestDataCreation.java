package com.webengage.ui_automation.pages;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.ExcelReader;
import com.webengage.ui_automation.utils.RuntimeUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import com.webengage.ui_automation.utils.SetupUtility;

public class TestDataCreation extends SeleniumExtendedUtility {
	SetupUtility setupUtility = new SetupUtility();
	APIOperationsPage apiOperations = new APIOperationsPage();
	RuntimeUtility runtimeUtility = new RuntimeUtility();
	ExcelReader excelReader=new ExcelReader();
	APIUtility apiUtility=new APIUtility();

	public void selectFunction(String function) {
		try {
			runtimeUtility.setDynamicRequestLicenseCode("Body", SetupUtility.licenseCode);
			apiOperations.create_data_api_operation(function);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void fetchAPIKeys() {
		try {
			setupUtility.setAuthApiKey(fetchText(CommonPageElements.API_KEY.locate()));
			setupUtility.setLicenseCode(fetchText(CommonPageElements.LICENSE_CODE.locate()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createUsers() {
		XSSFSheet sheetObj;
		try {
			sheetObj = setupUtility.readFromExcelSheet("CreateUser");
			LinkedList<HashMap<String, HashMap<String, String>>> userList = excelReader.processExcelSheetRowData(sheetObj);
			for(HashMap<String, HashMap<String, String>> user:userList) {
				apiOperations.user_creation_api_operations(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createCampaigns(String channel) {
		runtimeUtility.setDynamicRequestLicenseCode("Body", SetupUtility.licenseCode);
		try {
			apiOperations.create_data_campaigns_api_operation(channel);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
