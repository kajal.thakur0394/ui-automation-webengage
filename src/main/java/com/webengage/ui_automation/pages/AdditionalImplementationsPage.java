package com.webengage.ui_automation.pages;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencsv.CSVWriter;
import com.webengage.ui_automation.elements.AdditionalPageElements;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.SMSCampaignPageElements;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.ReflectionsUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class AdditionalImplementationsPage extends SeleniumExtendedUtility {
	private static final String CSV_FILE_PATH = "/src/test/resources/testData/campaignStats";
	static JSONObject urlTemplate;
	boolean isHeaderPresent = false;
	boolean isSummaryAdded = false;
	boolean isDataAvailable = false;
	boolean isdtableDataAvailable = false;
	boolean executeFordataTable = false;
	String url, type, campaignID, channel = null;

	// Objects for storing data of every state.
	Set<String> csvHeaderSet = new TreeSet<>(
			Arrays.asList("CampaignID", "Channel", "Env", "No of days", "Type", "VB1", "VB2", "View By"));
	Map<String, String> keyMetricsMap = new HashMap<>();
	Map<String, String> summaryMap = new HashMap<>();
	Map<String, String> dataTableMap = new HashMap<>();
	Map<String, String> campaignListPageMap = new HashMap<>();

	// Objects to be used in add to CSV line
	List<Map<String, String>> keyMetricsList = new ArrayList<>();
	List<Map<String, String>> summaryList = new ArrayList<>();
	List<Map<String, String>> dataTableList = new ArrayList<>();
	List<Map<String, String>> campaignListPageList = new ArrayList<>();
	List<String[]> noDataLinesList = new ArrayList<>();

	List<String[]> csvLines = new ArrayList<>();
	Map<String, String[]> prodCSVLineMap = new HashMap<>();
	Map<String, String[]> betaCSVLineMap = new HashMap<>();

	public AdditionalImplementationsPage() {
		if (urlTemplate == null)
			urlTemplate = new ReflectionsUtility().fetchTemplate(null, "CampaignStats", "campaignURLs");
	}

	public void sourceStats(String type, String channel) {
		this.channel = channel;
		this.type = type;
		fetchAllInputURLS();
	}

	/*
	 * This method will fetch the input from JSON file, and for every URL it will
	 * fetch the data and store it in a CSV.
	 */
	private void fetchAllInputURLS() {
		try {
			JSONArray urlArray = (JSONArray) ((JSONObject) urlTemplate.get("channel")).get(channel.toUpperCase());
			// Iterating the contents of the array
			boolean viewByFilterEnable = (boolean) urlTemplate.get("viewByFilter");
			for (int i = 0; i < urlArray.size(); i++) {
				String[] urls = { (String) urlArray.get(i), createBetaURL((String) urlArray.get(i)) };
				for (String url : urls) {
					this.url = url;
					openURL(url);
					Log.info("URL loaded:" + url);
					initializeExcelHeaders();
					Log.info("Selecting day..");
					selectDayFromURL(viewByFilterEnable);
					Log.info("Fetching campaign list page data..");
					fetchCampaignListData();
					Log.info("Campaign list page data successfully fetched");
				}
				createCSVData();
			}
			if (urlArray.size() > 0) {
				writeToCSV(channel);
			}
		} catch (Exception e) {
			Log.info("Inside catch block");
			createCSVData();
			writeToCSV(channel);
			Log.info("CSV with incomplete data generated");
			Log.info("Exception in fetching all input URLs");
			e.printStackTrace();
			CustomAssertions.assertFalse("Exception in fetching all input URLs", true);
		}
	}

	private void createCSVData() {
		addToCSVLines();
		mergeProdAndBetaCSVLineMap(prodCSVLineMap, betaCSVLineMap);
		for (String[] noDataLine : noDataLinesList) {
			csvLines.add(noDataLine);
		}
		Log.info("All Data Successfully fetched");
		noDataLinesList.clear();

	}

	private Map<String, Integer> fetchCampaignListHeader() {
		List<WebElement> campNameListHeader = findElements(AdditionalPageElements.CAMPAIGNLIST_HEADER.locate());
		Map<String, Integer> columnHeaderMap = new HashMap<>();
		for (int i = 0; i < campNameListHeader.size(); i++) {
			if (campNameListHeader.get(i).getText().equalsIgnoreCase("Campaign Name")) {
				continue;
			}
			columnHeaderMap.put(campNameListHeader.get(i).getText(), i);
		}
		return columnHeaderMap;
	}

	private void fetchCampaignListData() throws InterruptedException {
		waitForElementToBeVisible(AdditionalPageElements.CAMPAIGN_NAME.locate());
		String campaignName = fetchText(AdditionalPageElements.CAMPAIGN_NAME.locate());
		Log.info("CampaignList Page-campaignName:" + campaignName);
		click(AdditionalPageElements.CHANNEL_SELECT.locate());
		searchCamp(campaignName);
		Thread.sleep(2000);
		fetchCampaignListRowData(campaignName);

	}

	private void searchCamp(String campaignName) throws InterruptedException {
		if (checkIfElementisLocated(SMSCampaignPageElements.SEARCH_INPUT_BAR_VALUE.locate()))
			click(SMSCampaignPageElements.CLEAR_SEARCH.locate());
		sendKeys(SMSCampaignPageElements.SEARCH_INPUT_BAR.locate(), campaignName + Keys.RETURN);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(180));
		wait.until(ExpectedConditions.presenceOfElementLocated(SMSCampaignPageElements.TABLE_HEADER.locate()));

	}

	private void fetchCampaignListRowData(String campaignName) throws InterruptedException {
		scrollIntoView(AdditionalPageElements.NAME_LINK.locate());
		List<WebElement> campNameLinkList = findElements(AdditionalPageElements.NAME_LINK.locate());
		int i = 0;
		while (i < campNameLinkList.size()) {
			campNameLinkList.get(i).click();
			if (getCurrentURL().contains("overview")
					&& campaignListPageMap.get("CampaignID").contentEquals(fetchCampIDFromURL(url))) {
				click(AdditionalPageElements.CHANNEL_SELECT.locate());
				searchCamp(campaignName);
				List<WebElement> tdList = findElements(
						dynamicXpathLocator(AdditionalPageElements.CAMPAIGNLIST_ROWDATA, String.valueOf(i + 1)));
				fetchCampaignListHeader().entrySet().forEach(entry -> {
					campaignListPageMap.put(entry.getKey(), tdList.get(entry.getValue()).getText().isEmpty() ? "-"
							: tdList.get(entry.getValue()).getText().split("\n")[0]);
				});

			}

			else {
				click(AdditionalPageElements.CHANNEL_SELECT.locate());
				searchCamp(campaignName);
			}
			i++;
			campNameLinkList = findElements(AdditionalPageElements.NAME_LINK.locate());
		}

		csvHeaderSet.addAll(campaignListPageMap.keySet());
		addMapDataToList(campaignListPageMap, campaignListPageList);
	}

	private void mergeProdAndBetaCSVLineMap(Map<String, String[]> prodCSVLineMap,
			Map<String, String[]> betaCSVLineMap) {
		prodCSVLineMap.entrySet().removeIf(entry -> {
			if (betaCSVLineMap.containsKey(entry.getKey())) {
				csvLines.add(entry.getValue());
				csvLines.add(betaCSVLineMap.remove(entry.getKey()));
				return true;
			} else {
				return false;
			}
		});
		csvLines.addAll(prodCSVLineMap.values());
		csvLines.addAll(betaCSVLineMap.values());
		prodCSVLineMap.clear();
		betaCSVLineMap.clear();

	}

	/* This method will set the constant header values. */
	private void initializeExcelHeaders() {
		keyMetricsMap.put("CampaignID", fetchCampIDFromURL(url));
		summaryMap.put("CampaignID", fetchCampIDFromURL(url));
		dataTableMap.put("CampaignID", fetchCampIDFromURL(url));
		campaignListPageMap.put("CampaignID", fetchCampIDFromURL(url));
		keyMetricsMap.put("Channel", channel);
		summaryMap.put("Channel", channel);
		dataTableMap.put("Channel", channel);
		campaignListPageMap.put("Channel", channel);
		keyMetricsMap.put("Type", type);
		summaryMap.put("Type", "KM");
		dataTableMap.put("Type", "DataTable");
		campaignListPageMap.put("Type", "ListPage");
		if (url.contains("beta")) {
			keyMetricsMap.put("Env", "Beta");
			summaryMap.put("Env", "Beta");
			dataTableMap.put("Env", "Beta");
			campaignListPageMap.put("Env", "Beta");
		} else {
			keyMetricsMap.put("Env", "Prod");
			summaryMap.put("Env", "Prod");
			dataTableMap.put("Env", "Prod");
			campaignListPageMap.put("Env", "Prod");
		}
	}

	private String fetchCampIDFromURL(String url) {
		return url.replace("overview", "").split("campaigns")[1].replace("/", "");
	}

	private void selectDayFromURL(boolean viewByFilterEnable) throws Exception {
		JSONArray daysList = (JSONArray) urlTemplate.get("timePeriod");
		for (int i = 0; i < daysList.size(); i++) {
			selectDay((String) daysList.get(i), viewByFilterEnable);
		}
	}

	private void selectViewAs() throws Exception {
		click(AdditionalPageElements.VIEWAS_MENU.locate());
		click(AdditionalPageElements.COUNT_VIEWAS.locate());
		Log.info("COUNT view as successfully selected.");
	}

	private void selectDay(String selectedDay, boolean viewByFilterEnable) throws Exception {
		scrollIntoView(AdditionalPageElements.DATE.locate());
		click(AdditionalPageElements.DATE.locate());
		if (selectedDay.contains("-"))
			new CommonPage().selectCustomTimePeriod(selectedDay);
		else
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, selectedDay));
		keyMetricsMap.put("No of days", selectedDay);
		summaryMap.put("No of days", selectedDay);
		dataTableMap.put("No of days", selectedDay);
		Log.info("Day selected:" + selectedDay);
		selectAndSaveCheckBoxDetails();
		Log.info("Fetching Key Metrics data...");
		fetchSummaryDetails();
		Log.info("Key Metrics data fetched successfully.");
		selectViewAs();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		processViewBy(viewByFilterEnable);
		Log.info("Detailed Metrics data fetched successfully.");
		operateOnDataTable();
		Log.info("Datable's data fetched successfully.");
		scrollIntoView(AdditionalPageElements.DATE.locate());
		click(AdditionalPageElements.DATE.locate());

	}

	/* This method will select all the check boxes of the Detailed Metrics card. */
	private void selectAndSaveCheckBoxDetails() throws Exception {
		click(AdditionalPageElements.FILTER.locate());
		List<WebElement> list1 = findElements(AdditionalPageElements.COLUMN_LABEL.locate());

		for (WebElement element : list1) {
			if (element.getAttribute("class").contains("is-checked")) {
				continue;
			}
			element.click();
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
	}

	/*
	 * This method will select all the data from the Key Metrics card and will add
	 * it in a list.
	 */
	private void fetchSummaryDetails() throws Exception {
		List<WebElement> allLabelList = findElements(AdditionalPageElements.SUMMARY_ALL_LABELS.locate());
		List<WebElement> filterLabelList = findElements(AdditionalPageElements.SUMMARY_LABEL1.locate());
		if (filterLabelList.size() == 0) {
			for (WebElement element : allLabelList) {
				addTitleValueInSummaryMap(element.getText(), summaryMap, allLabelList);
			}
		} else {
			List<WebElement> filterLabelValue = findElements(AdditionalPageElements.SUMMARY_VALUE_COUNT1.locate());
			for (int i = 0; i < filterLabelValue.size(); i++) {
				summaryMap.put(filterLabelList.get(i).getText(), filterLabelValue.get(i).getText());
			}
			for (WebElement element : allLabelList) {
				if (!summaryMap.containsKey(element.getText())) {
					addTitleValueInSummaryMap(element.getText(), summaryMap, allLabelList);
				}
			}
		}
		summaryMap = addSubTitleValueInSummaryMap(summaryMap);
		csvHeaderSet.addAll(summaryMap.keySet());
		addMapDataToList(summaryMap, summaryList);
	}

	private void addMapDataToList(Map<String, String> map, List<Map<String, String>> list) {
		Map<String, String> tempMap = new HashMap<>();
		tempMap.putAll(map);
		list.add(tempMap);
	}

	private Map<String, String> addSubTitleValueInSummaryMap(Map<String, String> summaryMap) throws Exception {
		Thread.sleep(2000);
		List<WebElement> allSubtitles = findElements(AdditionalPageElements.SUMMARY_ALL_SUBTITLES.locate());
		for (WebElement element : allSubtitles) {
			summaryMap.put(element.getText(),
					fetchText(dynamicXpathLocator(AdditionalPageElements.SUMMARY_SUBTITLE_VALUE, element.getText())));
		}
		return summaryMap;
	}

	private void addTitleValueInSummaryMap(String elementText, Map<String, String> summaryMap,
			List<WebElement> allLabelList) throws Exception {
		Thread.sleep(2000);
		if (elementText.trim().equalsIgnoreCase("SENT") || elementText.trim().equalsIgnoreCase("DELIVERED")) {
			summaryMap.put(elementText, fetchText(
					dynamicXpathLocator(AdditionalPageElements.SUMMARY_ALL_VALUE, elementText.trim().concat(" "))));
		} else {
			summaryMap.put(elementText,
					fetchText(dynamicXpathLocator(AdditionalPageElements.SUMMARY_ALL_VALUE, elementText)));
		}
	}

	private String createBetaURL(String url) throws Exception {
		return url.split("dashboard")[0].concat("beta-dashboard").concat(url.split("dashboard")[1]);
	}

	private void processViewBy(boolean viewByFilterEnable) throws Exception {
		TreeSet<String> viewByVariations = new TreeSet<>();
		Log.info("Fetching Detailed Stats now...");
		switch (channel) {
		case "SMS":
		case "Email":
		case "Whatsapp":
			keyMetricsMap.put("View By", "-");
			dataTableMap.put("View By", "-");
			campaignListPageMap.put("View By", "-");
			Log.info("Fetching Detailed Stats for View By as ".concat("-"));
			processKeyMetrics();
			Log.info("Successfully fetched detailed Stats for View By as ".concat("-"));
			break;
		default:
			click(AdditionalPageElements.VB1.locate());
			List<WebElement> VB1List = findElements(AdditionalPageElements.VB1_DOWN.locate());
			int i = 0;
			while (i < VB1List.size()) {
				VB1List.get(i).click();
				String vb1 = fetchText(AdditionalPageElements.VB1.locate());
				keyMetricsMap.put("View By", vb1);
				Log.info("VB1-".concat(vb1));
				Log.info("Fetching Detailed Stats for:".concat("VB1-").concat(vb1));
				processKeyMetrics();
				Log.info("Detailed Stats fetched successfully for:".concat("VB1-").concat(vb1));
				click(AdditionalPageElements.VB2.locate());
				List<WebElement> VB2List = findElements(AdditionalPageElements.VB2_DOWN.locate());
				int j = 0;
				while (j < VB2List.size() && viewByFilterEnable) {
					VB2List.get(j).click();
					String vb2 = fetchText(AdditionalPageElements.VB2.locate());
					String combinationToBeProcessed;
					if (vb1.compareToIgnoreCase(vb2) < 0)
						combinationToBeProcessed = vb1 + "," + vb2;
					else
						combinationToBeProcessed = vb2 + "," + vb1;
					if (!viewByVariations.contains(combinationToBeProcessed)) {
						viewByVariations.add(combinationToBeProcessed);
						keyMetricsMap.put("View By", fetchText(AdditionalPageElements.VB1.locate()).concat(" and ")
								.concat(fetchText(AdditionalPageElements.VB2.locate())));
						Log.info("VB2-".concat(vb2));
						Log.info("Fetching Detailed Stats for:".concat("VB1-").concat(vb1).concat(" and VB2-")
								.concat(vb2));
						processKeyMetrics();
						Log.info("Detailed Stats fetched successfully for:".concat("VB1-").concat(vb1)
								.concat(" and VB2-").concat(vb2));
					} else
						Log.info("Combination has already been covered, skipping the same" + combinationToBeProcessed);
					click(AdditionalPageElements.CLEAR_SELECTION.locate());
					click(AdditionalPageElements.VB2.locate());
					VB2List.clear();
					VB2List = findElements(AdditionalPageElements.VB2_DOWN.locate());
					j++;
				}
				click(AdditionalPageElements.VB1.locate());
				VB1List.clear();
				VB1List = findElements(AdditionalPageElements.VB1_DOWN.locate());
				i++;
			}
			break;
		}
	}

	/*
	 * This method will select all the data from the Detailed Stats card and will
	 * add it in a list.
	 */
	private void processKeyMetrics() throws Exception {
		List<WebElement> trList = findElements(AdditionalPageElements.TABLE_ROWS.locate());
		if (trList.size() == 0) {
			Log.info("No Data Available for Detailed Stats.");
			noDataLinesList.add(new String[] { "No Data Available for url: " + url + " (Environment:- "
					+ keyMetricsMap.get("Env") + ", No.of days:- " + keyMetricsMap.get("No of days") + ", Type:- "
					+ keyMetricsMap.get("Type") + ", View By:- " + keyMetricsMap.get("View By") + ")" });
		} else {
			for (int i = 0; i < trList.size(); i++) {
				List<WebElement> tdList = findElements(
						dynamicXpathLocator(AdditionalPageElements.ROW_TD, String.valueOf(i + 1)));
				checkVBType(i + 1);
				createExcelColumnHeaders().entrySet().forEach(entry -> {
					keyMetricsMap.put(entry.getKey(), tdList.get(entry.getValue()).getText().isEmpty() ? "-"
							: tdList.get(entry.getValue()).getText().split("\n")[0]);
				});
				csvHeaderSet.addAll(keyMetricsMap.keySet());
				addMapDataToList(keyMetricsMap, keyMetricsList);
			}
		}
	}

	private Map<String, Integer> createExcelColumnHeaders() throws Exception {
		List<WebElement> list = findElements(AdditionalPageElements.TABLE_SHORT_COL.locate());
		Map<String, Integer> columnHeaderMap = new HashMap<>();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getText().equalsIgnoreCase("Type") || list.get(i).getText().equalsIgnoreCase("Layout"))
				continue;
			columnHeaderMap.put(list.get(i).getText(), i);
		}
		return columnHeaderMap;
	}

	private void checkVBType(int index) throws Exception {
		if (checkIfElementisLocated(
				dynamicXpathLocator(AdditionalPageElements.CURSOR_POINTER, String.valueOf(index)))) {
			keyMetricsMap.put("VB1", fetchAttributeValue(
					dynamicXpathLocator(AdditionalPageElements.CURSOR_POINTER, String.valueOf(index)), "innerText"));
			keyMetricsMap.put("VB2", "-");
		} else if (checkIfElementisLocated(
				dynamicXpathLocator(AdditionalPageElements.VB2TEXT, String.valueOf(index)))) {
			keyMetricsMap.put("VB2", fetchAttributeValue(
					dynamicXpathLocator(AdditionalPageElements.VB2TEXT, String.valueOf(index)), "innerText"));
		} else if (checkIfElementisLocated(dynamicXpathLocator(AdditionalPageElements.VB1TEXT, String.valueOf(index)))
				&& (!fetchAttributeValue(dynamicXpathLocator(AdditionalPageElements.VB1TEXT, String.valueOf(index)),
						"innerText").isEmpty())) {
			keyMetricsMap.put("VB1", fetchAttributeValue(
					dynamicXpathLocator(AdditionalPageElements.VB1TEXT, String.valueOf(index)), "innerText"));
			keyMetricsMap.put("VB2", "-");
		} else {

			keyMetricsMap.put("VB1", fetchAttributeValue(
					dynamicXpathLocator(AdditionalPageElements.VB1ALTERNATETEXT, String.valueOf(index)), "innerText"));
			keyMetricsMap.put("VB2", "-");
		}
	}

	private void addToCSVLines() {
		List<String> csvData = new ArrayList<String>();
		if (!isHeaderPresent) {
			csvLines.add(csvHeaderSet.toArray(new String[0]));
			isHeaderPresent = true;
		}

		extractDataFromList(summaryList, csvData);
		extractDataFromList(keyMetricsList, csvData);
		extractDataFromList(dataTableList, csvData);
		extractDataFromList(campaignListPageList, csvData);

	}

	private void extractDataFromList(List<Map<String, String>> dataList, List<String> csvData) {
		for (Map<String, String> map : dataList) {

			String hashKey = String.join("-", map.get("CampaignID"), map.get("Type"), map.get("No of days"),
					map.get("View By"), map.get("VB1"), map.get("VB2"), map.get("DATE"));
			csvHeaderSet.forEach(header -> {
				csvData.add(map.get(header) == null ? "-" : map.get(header));
			});
			if (map.get("Env").equalsIgnoreCase("Prod")) {
				prodCSVLineMap.put(hashKey, csvData.toArray(new String[0]));
			} else {
				betaCSVLineMap.put(hashKey, csvData.toArray(new String[0]));
			}
			csvData.clear();
		}
		dataList.clear();
	}

	private void writeToCSV(String fileName) {
		try {
			Log.info("Writing into CSV now...");
			String directoryName = System.getProperty("user.dir") + CSV_FILE_PATH;
			String filePath = directoryName + "/" + fileName + ".csv";
			File directory = new File(directoryName);
			if (!directory.exists()) {
				directory.mkdirs();
			}

			File file = new File(filePath);
			FileWriter outputfile = new FileWriter(file);
			CSVWriter writer = new CSVWriter(outputfile);
			csvLines.forEach(line -> {
				writer.writeNext(line);
			});
			Log.info("Sucessfully entered data into CSV.");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * This method will select all the data from the DataTable card and will add it
	 * in a list.
	 */
	private void operateOnDataTable() throws Exception {
		Log.info("Fetching data from Variation DataTable...");
		scrollIntoView(AdditionalPageElements.DATATABLE_MENU.locate());
		click(AdditionalPageElements.DATATABLE_MENU.locate());
		scrollIntoView(AdditionalPageElements.TABLE_OPTION.locate());
		click(AdditionalPageElements.TABLE_OPTION.locate());
		List<WebElement> dtRowList = findElements(AdditionalPageElements.DATATABLE_ROWS.locate());
		if (dtRowList.size() == 0) {
			Log.info("No Data Available for Variation DataTable.");
			noDataLinesList.add(new String[] { "No Data Available for url: " + url + " (Environment:- "
					+ dataTableMap.get("Env") + ", No.of days:- " + dataTableMap.get("No of days") + ", Type:- "
					+ dataTableMap.get("Type") + ", VB1:- " + dataTableMap.get("VB1") + ")" });
		} else {
			scrollIntoView(AdditionalPageElements.DATATABLE_VB1_GETLIST.locate());
			click(AdditionalPageElements.DATATABLE_VB1_GETLIST.locate());
			List<WebElement> vb1List = findElements(AdditionalPageElements.DATATABLE_VB1_LIST.locate());
			int i = 0;
			while (i < vb1List.size()) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click();", vb1List.get(i));
				dataTableMap.put("VB1",
						fetchAttributeValue(AdditionalPageElements.DATATABLE_VB1_GETLIST.locate(), "innerText"));
				dataTableMap.put("VB2", "-");
				if (checkIfElementisLocated(AdditionalPageElements.PAGINATION_NEXT.locate())) {
					checkPagination();
				} else {
					fetchDataFromTable();
				}
				scrollIntoView(AdditionalPageElements.DATATABLE_VB1_GETLIST.locate());
				click(AdditionalPageElements.DATATABLE_VB1_GETLIST.locate());
				i++;
				vb1List = findElements(AdditionalPageElements.DATATABLE_VB1_LIST.locate());
			}
		}
	}

	private void checkPagination() throws Exception {
		fetchDataFromTable();
		if (checkIfElementisLocated(AdditionalPageElements.PAGINATION_NEXT.locate())) {
			click(AdditionalPageElements.PAGINATION_NEXT.locate());
			checkPagination();
		} else {
			Log.info("Pagination no more exist");
			Log.info("Going back to the start of the page..");
			checkPrevPagination();
		}

	}

	private void checkPrevPagination() {
		if (checkIfElementisLocated(AdditionalPageElements.PAGINATION_PREV.locate())) {
			click(AdditionalPageElements.PAGINATION_PREV.locate());
			checkPrevPagination();
		} else {
			Log.info("Back to Page-1");
		}

	}

	private void fetchDataFromTable() throws Exception {
		List<WebElement> dtheaderList = findElements(AdditionalPageElements.DATATABLE_HEADERS.locate());
		List<WebElement> dtRowList = findElements(AdditionalPageElements.DATATABLE_ROWS.locate());
		Map<String, Integer> dtColumnHeaderMap = new HashMap<>();
		if (dtRowList.size() == 0 || dtheaderList.size() == 0) {
			noDataLinesList.add(new String[] { "No Data Available for url: " + url + " (Environment:- "
					+ dataTableMap.get("Env") + ", No.of days:- " + dataTableMap.get("No of days") + ", Type:- "
					+ dataTableMap.get("Type") + ", VB1:- " + dataTableMap.get("VB1") + ")" });
		} else {
			// add data in headers
			for (int i = 0; i < dtheaderList.size(); i++) {
				if (dtColumnHeaderMap.containsKey(dtheaderList.get(i).getText())) {
					continue;
				}
				dtColumnHeaderMap.put(dtheaderList.get(i).getText(), i);
			}
			// Add data in row
			for (int i = 0; i < dtRowList.size(); i++) {
				List<WebElement> dtList = findElements(
						dynamicXpathLocator(AdditionalPageElements.DATATABLE_TDS, String.valueOf(i + 1)));
				dtColumnHeaderMap.entrySet().forEach(entry -> {
					dataTableMap.put(entry.getKey(), dtList.get(entry.getValue()).getText().isEmpty() ? "-"
							: dtList.get(entry.getValue()).getText().split("\n")[0]);

				});
				csvHeaderSet.addAll(dataTableMap.keySet());
				addMapDataToList(dataTableMap, dataTableList);
			}
		}
	}
}
