package com.webengage.ui_automation.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.webengage.ui_automation.utils.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.webengage.ui_automation.driver.DataFactory;
import com.webengage.ui_automation.elements.CampaignsPageElements;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.IntegrationsPageElements;
import com.webengage.ui_automation.elements.JourneysPageElements;
import com.webengage.ui_automation.elements.SMSCampaignPageElements;
import com.webengage.ui_automation.elements.SegmentsPageElements;
import com.webengage.ui_automation.utils.ConstantUtils;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.NotificationToastMessages;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class SegmentsPage extends SeleniumExtendedUtility {
	CommonPage comPage = new CommonPage();
	String segmentName;
	String userCount;
	
	public static void setSegmentId(String segmentID) {
		DataFactory.getInstance().setData(ConstantUtils.SEGMENTID, segmentID);
	}
	public static String getSegmentId() {
		return DataFactory.getInstance().getData(ConstantUtils.SEGMENTID, String.class);
	}

	public String getSegmentName() {
		return segmentName;
	}

	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	public String getUserCount() {
		return userCount;
	}

	public void setUserCount(String userCount) {
		this.userCount = userCount;
	}

	public SegmentsPage() {
		super();
	}

	public void openSegmentType(String segType) {

		if (!fetchAttributeValue(SegmentsPageElements.SEGMENTS_TAB_LIST.locate(), "class").contains("active"))
			click(SegmentsPageElements.SEGMENTS_TAB.locate());
		if (segType.equalsIgnoreCase("Live Segments")) {
			click(SegmentsPageElements.LIVE_SEGMENTS_TAB.locate());
		} else {
			click(SegmentsPageElements.STATIC_LISTS_TAB.locate());
		}

	}

	public void newSegment(String segName) {
		click(SegmentsPageElements.CREATE_BTN.locate());
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Segment Name"), segName);
		setSegmentName(segName);
	}

	public void fillChoicesUserCard(List<List<String>> choices) {
		boolean collasped = fetchAttributeValue(SegmentsPageElements.USER_CARD_HEADER.locate(), "class")
				.contains("collapsed");
		if (collasped)
			click(SegmentsPageElements.USER_CARD.locate());
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			String valueCol2 = "";
			String text = "";
			String date = "";
			String month = "";
			if (value.contains(">")) {
				text = value.split(">")[1];
				value = value.split(">")[0];
			}
			if (value.contains("-")) {
				valueCol2 = value.split("-")[1];
				value = value.split("-")[0];
			}
			if (parameter.equalsIgnoreCase("Set Date as")) {
				date = value.split(" ")[0];
				month = value.split(" ")[1];
				setCalendarDate(date, month);
			} else if (parameter.contains("-")) {
				valueCol2 = parameter.split("-")[1];
				parameter = parameter.split("-")[0];
				sendKeys(dynamicXpathLocator(SegmentsPageElements.SEND_VALUE, parameter, valueCol2), value);
			} else {
				By dd_Type = parameter.contains(">")
						? dynamicXpathLocator(SegmentsPageElements.OPEN_DD_SELECT, parameter.split(">")[0],
								parameter.split(">")[1])
						: dynamicXpathLocator(SegmentsPageElements.OPEN_DD, parameter);
				click(dd_Type);
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, value));
				if (!text.isEmpty())
					sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Enter a value"), text);
				if (!valueCol2.isEmpty()) {
					click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD_COL2, parameter));
					click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, valueCol2));
				}
			}
		}
	}

	public void setCalendarDate(String date, String month) {
		click(SegmentsPageElements.OPEN_CALENDAR.locate());
		click(SegmentsPageElements.OPEN_MONTH_LIST.locate());
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_MONTH, month));
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_DATE, date, month));
	}

	public void checkSegementDetails(String type, Integer expectedValue) {
		setUserCount(String.valueOf(expectedValue));
		waitForSpinner();
		click(SegmentsPageElements.SEGMENT_DETAILS_BTN.locate());
		String actualValue = fetchText(dynamicXpathLocator(SegmentsPageElements.SEGEMENT_DETAIL_COUNT, type));
		CustomAssertions.assertEquals("Segment count do not match:", Double.valueOf(expectedValue),
				Double.valueOf(actualValue));
	}

	public void saveSegment() {
		click(SegmentsPageElements.SAVE_BTN.locate());
	}

	public void checkList(String type, boolean liveSeg) {
		switch (type) {
		case "Updated":
			verifyToastMessage(NotificationToastMessages.SEGMENT_UPDATED.message());
			break;
		case "Created":
			verifyToastMessage(NotificationToastMessages.SEGMENT_CREATED.message());
			break;
		default:
			break;
		}
		if (liveSeg)
			waitForElementToBeVisible(dynamicXpathLocator(SegmentsPageElements.SEGMENTS_LIST, getSegmentName()));
	}

	public void checkList(String segName) {
		waitForElementToBeVisible(dynamicXpathLocator(SegmentsPageElements.SEGMENTS_LIST, segName));
	}

	public void deleteSegment(String segName, String type, String toastMsg) {
		setSegmentName(segName);
		waitUntilAttributeDoesNotContain(SegmentsPageElements.CARD_CONTENT.locate(), "class", "loader");
		clearFieldAndSendkeys(SMSCampaignPageElements.SEARCH_SEGMENTNAME.locate(), segmentName + Keys.RETURN);
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_POP_OVER, segName));
		click(SegmentsPageElements.POP_OVER_DELETE.locate());
		click(dynamicXpathLocator(CommonPageElements.BUTTON, type));
		verifyToastMessage(toastMsg);
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		click(JourneysPageElements.CLOSE_BTN.locate());
	}

	public void checkIfNotInList() {
		waitForSpinner();
		waitForElementToBeInvisible(dynamicXpathLocator(SegmentsPageElements.SEGMENTS_LIST, getSegmentName()));
	}

	public void performActionforSegment(String action, String segment) {
		setSegmentName(segment);
		waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
		clearFieldAndSendkeys(SMSCampaignPageElements.SEARCH_SEGMENTNAME.locate(), segment + Keys.RETURN);
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_POP_OVER, segment));
		click(dynamicXpathLocator(SegmentsPageElements.PERFORM_ACTION, action));
	}

	public void duplicateContainer() {
		CustomAssertions.assertEquals("Duplicate String does not match:", "Duplicate Segment",
				fetchText(SegmentsPageElements.CONTAINER_TITLE.locate()));
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD, "Project"));
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, System.getProperty("accountName")));
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Duplicate"));
		verifyToastMessage(NotificationToastMessages.CLONE_SEGMENT.message());
	}

	public void openSegment(String segName) {
		setSegmentName(segName);
		waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
		clearFieldAndSendkeys(SMSCampaignPageElements.SEARCH_SEGMENTNAME.locate(), segName + Keys.RETURN);
		clickUsingJSExecutor(dynamicXpathLocator(SegmentsPageElements.OPEN_SEGMENT, segName));
		waitForElementToBePresent(dynamicXpathLocator(SegmentsPageElements.TOOLTIP_BUTTON, "Edit"));
		waitForElementToBePresent(SegmentsPageElements.SEGEMNT_PAGE_HEADER.locate());
		CustomAssertions.assertEquals("Segments do not match:", segName,
				fetchAttributeValue(SegmentsPageElements.SEGEMNT_PAGE_HEADER.locate(), "title"));
	}

	public void clickOnButton(String button) {
		click(dynamicXpathLocator(CommonPageElements.BUTTON, button));
	}

	public void clickOnToolTipButton(String button) {
		click(dynamicXpathLocator(SegmentsPageElements.TOOLTIP_BUTTON, button));
	}

	public void fillChoicesBehCard(List<List<String>> choices) throws InterruptedException {
		boolean collasped = fetchAttributeValue(SegmentsPageElements.BEHAVIORAL_CARD_HEADER.locate(), "class")
				.contains("collapsed");
		if (collasped)
			click(SegmentsPageElements.BEHAVIORAL_CARD.locate());
		for (List<String> choice : choices) {
			String logicalOp = choice.get(0);
			String parameter = choice.get(1);
			String value = choice.get(2);
			if (logicalOp != null) {
				TimeUnit.MILLISECONDS.sleep(2500);
				click(dynamicXpathLocator(SegmentsPageElements.ADD_BUTTON, parameter));
			}
			TimeUnit.MILLISECONDS.sleep(1500);
			click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD_SELECT, parameter, "-"));
			sendKeys(CommonPageElements.SEARCH.locate(), value.split(">")[1]);
			String traverseDirection = "preceding";
			if (value.split(">")[0].toLowerCase().contains("system")) {
				traverseDirection = "following";
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_EVENT, traverseDirection, value.split(">")[1]));
			}
			if (value.split(">")[0].toLowerCase().contains("application")) {
				traverseDirection = "following";
				click(dynamicXpathLocator(SegmentsPageElements.SELECT_APPLICATION_EVENT, traverseDirection,
						value.split(">")[1]));
			}
			if (logicalOp != null) {
				List<WebElement> el = findElements(
						dynamicXpathLocator(SegmentsPageElements.LOGICAL_OP, parameter, logicalOp));
				el.get(el.size() - 1).click();
			}
		}
	}

	public void verifyUsersCount(String value, Integer expectedValue) {
		String actualValue = fetchText(dynamicXpathLocator(SegmentsPageElements.USERS_SEGMENT_PAGE, value));
		CustomAssertions.assertEquals("User count do not match:", Double.valueOf(expectedValue),
				Double.valueOf(actualValue));
	}

	public void fillChoicesTechCard(String type, List<List<String>> choices) {
		boolean collasped = fetchAttributeValue(SegmentsPageElements.TECHNOLOGY_CARD_HEADER.locate(), "class")
				.contains("collapsed");
		if (collasped)
			click(SegmentsPageElements.TECHNOLOGY_CARD.locate());
		fillCardsinTechnology(type, choices);
	}

	private void fillCardsinTechnology(String type, List<List<String>> choices) {
		boolean collasped;
		switch (type) {
		case "Android":
			collasped = fetchAttributeValue(SegmentsPageElements.ANDROID_CARD_HEADER.locate(), "class")
					.contains("collapsed");
			if (collasped)
				click(SegmentsPageElements.ANDROID_CARD.locate());
			break;
		case "iOS":
			collasped = fetchAttributeValue(SegmentsPageElements.IOS_CARD_HEADER.locate(), "class")
					.contains("collapsed");
			if (collasped)
				click(SegmentsPageElements.IOS_CARD.locate());
			break;
		case "Web":
			collasped = fetchAttributeValue(SegmentsPageElements.WEB_CARD_HEADER.locate(), "class")
					.contains("collapsed");
			if (collasped)
				click(SegmentsPageElements.WEB_CARD.locate());
			break;
		default:
			break;
		}
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			String text = "";
			if (value.contains(">")) {
				text = value.split(">")[1];
				value = value.split(">")[0];
			}
			click(dynamicXpathLocator(SegmentsPageElements.SELECT_PLACE_HOLDER, type));
			click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, parameter));
			click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD, parameter));
			click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, value));
			if (!text.isEmpty()) {
				sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Enter a value"), text);
			}
		}
	}

	public void openUserTab(String tab) {
		click(SegmentsPageElements.USERS_TAB.locate());
		click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, tab));
		waitForElementToBeVisible(SegmentsPageElements.TABLE_HEADER.locate());
	}

	public void searchBar(String keyword) {
		clearTextField(SegmentsPageElements.SEARCH_BAR.locate());
		findElement(SegmentsPageElements.SEARCH_BAR.locate()).sendKeys(keyword + Keys.RETURN);
		waitForElementToBeVisible(SegmentsPageElements.TABLE_HEADER.locate());
	}

	public void verifyUserRow(int expectedCount, String userID) {
		CustomAssertions.assertEquals("Row count do not match:", expectedCount,
				findElements(SegmentsPageElements.USER_ROWS.locate()).size());
		List<WebElement> userIds = findElements(SegmentsPageElements.USER_ROWS_USER_ID.locate());
		for (WebElement el : userIds) {
			CustomAssertions.assertEquals("ID count does not match:", userID, el.getAttribute("innerHTML"));
		}
	}

	public void selectFilters(List<String> list) {
		click(SegmentsPageElements.FILTER_BTN.locate());
		List<WebElement> selected = findElements(SegmentsPageElements.SELECTED_FILTERS.locate());
		for (WebElement temp : selected) {
			temp.click();
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
		List<WebElement> headers = findElements(SegmentsPageElements.TABLE_HEADERS.locate());
		CustomAssertions.assertEquals("Size do not match:", 1, headers.size());
		click(SegmentsPageElements.FILTER_BTN.locate());
		for (String s : list) {
			click(dynamicXpathLocator(SegmentsPageElements.CHECKBOX, s));
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
		headers = findElements(SegmentsPageElements.TABLE_HEADERS.locate());
		CustomAssertions.assertEquals("Size does not match:", list.size() + 1, headers.size());
	}

	public void verifyColumns(List<String> expectedColumns) {
		List<WebElement> headers = findElements(SegmentsPageElements.TABLE_HEADERS.locate());
		List<String> actualColumns = new ArrayList<>();
		for (WebElement el : headers) {
			actualColumns.add(el.getText());
		}
		Collections.sort(actualColumns, String.CASE_INSENSITIVE_ORDER);
		Collections.sort(expectedColumns, String.CASE_INSENSITIVE_ORDER);
		CustomAssertions.assertEquals("Columns do not match:", expectedColumns.toString().toLowerCase(),
				actualColumns.toString().toLowerCase());
	}

	public void newStaticSegment(String type, String name) {
		type = type.contains("CSV") ? "upload CSV file" : " Create with Editor ";
		click(SegmentsPageElements.CREATE_BTN.locate());
		click(dynamicXpathLocator(CommonPageElements.BUTTON, type));
		if (type.contains("CSV"))
			sendKeys(SegmentsPageElements.CSV_SEGMENT_NAME.locate(), name);
		else
			sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Segment Name"), name);
		setSegmentName(name);
	}

	public void verifyStaticSegments() {
		waitForElementToBeVisible(dynamicXpathLocator(SegmentsPageElements.OPEN_SEGMENT, getSegmentName()));
		boolean statusLabel = checkIfElementisLocated(
				dynamicXpathLocator(SegmentsPageElements.STATIC_SEGMENT_TOOL_TIP, getSegmentName()));
		if (statusLabel) {
			String actualValue = fetchText(
					dynamicXpathLocator(SegmentsPageElements.STATIC_SEGMENT_TOOL_TIP, getSegmentName()));
			CustomAssertions.assertEquals("Status do not match:", "IN PROGRESS", actualValue);
		} else
			CustomAssertions.assertTrue("Brewing tool tip not found", checkIfElementisLocated(
					dynamicXpathLocator(SegmentsPageElements.SEGMENT_TOOLTIP, getSegmentName())));
	}

	public void addFilter(String type, List<String> list) {
		List<WebElement> el = findElements(dynamicXpathLocator(SegmentsPageElements.FILTER_BUTTON, type));
		el.get(el.size() - 1).click();
		String expected = type.contains("NOT") ? "Filter for users who DID NOT do events"
				: "Filter for users who DID events";
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, expected));
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, "- Select Attribute -"));
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, list.get(0)));
		click(SegmentsPageElements.SELECT_OPERATOR.locate());
		click(dynamicXpathLocator(SegmentsPageElements.SELECT_FROM_DD, list.get(1)));
		click(SegmentsPageElements.ENTER_A_VALUE.locate());
		sendKeys(SegmentsPageElements.ENTER_VALUE.locate(), list.get(2) + Keys.RETURN);
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "APPLY"));
		waitForElementToBeInvisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, expected));
		el = findElements(dynamicXpathLocator(SegmentsPageElements.FILTER_BUTTON, type));
		CustomAssertions.assertEquals("Size does not match:", "1", el.get(el.size() - 1).getAttribute("data-count"));
	}

	public void uploadFile(String segName) {
		uploadFile("StaticList.csv", IntegrationsPageElements.UPLOAD.locate());
		clearFieldAndSendkeys(dynamicXpathLocator(CampaignsPageElements.INPUT_FIELD, "SEGMENT NAME"), segName);
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Save & Continue"));
	}

	/*
	 * @param{name} This method will verify that segment name in the list matches
	 * with the expected segment name.
	 */
	public void verifySegment(String name) {
		waitForElementToBeVisible(SMSCampaignPageElements.TABLE_HEADER.locate());
		clearFieldAndSendkeys(SMSCampaignPageElements.SEARCH_SEGMENTNAME.locate(), name + Keys.RETURN);
		CustomAssertions.assertEquals("Segments do not match:", name,
				fetchAttributeValue(CommonPageElements.NAME_LINK_TEXT.locate(), "title"));
	}

	public void openMeatBallTagMenu(String segmentName) {
		click(dynamicXpathLocator(SegmentsPageElements.OPEN_POP_OVER, segmentName));
		click(SegmentsPageElements.POP_OVER_TAG.locate());
	}

	public void verifyTagIsDeletedFromList() {
		verifyTagIsDeleted();

	}

	public void verifyTagIsDeletedFromList(String name) {
		verifyTagIsDeleted(name);
	}

//	public void verifyTagIsDeletedFromShowDetails() {
//		comPage.openCampaign();
//		click(SegmentsPageElements.SHOWDETAILS.locate());
//		verifyTagIsDeleted();
//
//	}

	private void verifyTagIsDeleted() {
		waitForElementToBeInvisible(SegmentsPageElements.TAG.locate());
		CustomAssertions.assertEquals("Element is not present:", false,
				checkIfElementisLocated(SegmentsPageElements.TAG.locate()));
	}

	private void verifyTagIsDeleted(String name) {
		waitForElementToBeInvisible(
				dynamicXpathLocator(SegmentsPageElements.TAG_DATE, name, CampaignsPage.getSystemDateTime()));
		CustomAssertions.assertEquals("Element is not present:", false, checkIfElementisLocated(
				dynamicXpathLocator(SegmentsPageElements.TAG_DATE, name, CampaignsPage.getSystemDateTime())));
	}

	public void openShowDetailsPage(String segName) {
		comPage.openCampaign(segName);
		click(SegmentsPageElements.SHOWDETAILS.locate());
		click(SegmentsPageElements.ADD_TAG_DETAILS.locate());

	}

	public void getSegmentID() {
		String segmentID = getCurrentURL().split("/")[getCurrentURL().split("/").length - 2];
		setSegmentId(segmentID);
		Log.info("SEGMENTID: " + segmentID);
		Log.info(getSegmentId());
	}

}
