package com.webengage.ui_automation.pages;

import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONObject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.OnsiteNotificationPageElements;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class OnsiteNotificationPage extends SeleniumExtendedUtility {
	APIOperationsPage apiOperationsPage = new APIOperationsPage();
	CommonPage comPage = new CommonPage();
	public static String notificationName = null;

	public String getNotificationName() {
		return notificationName;
	}

	public void setNotificationName(String notificationName) {
		OnsiteNotificationPage.notificationName = notificationName;
	}

	/**
	 * @param{campaignName} This method will locate the 'Search input' field and
	 *                      enters the campaign name inside the input field.
	 */
	public void searchBar(String campaignName) {
		clearTextField(CommonPageElements.SEARCH.locate());
		sendKeys(CommonPageElements.SEARCH.locate(), campaignName + Keys.RETURN);
	}

	/**
	 * @param{campaignName},{campaignType} This method will verify that
	 *                                     'CampaignName' and 'CampaignType'
	 *                                     searched matches with the expected
	 *                                     'CampaignName' and 'layoutType'.
	 */
	public void verifyCampaignNameandType(String campaignName, String layoutType) {
		CustomAssertions.assertEquals("CampaignName do not match:", campaignName,
				fetchText(OnsiteNotificationPageElements.NAME_TEXT.locate()));
		CustomAssertions.assertEquals("LayoutType do not match:", layoutType, getElementValue(layoutType));
		clearTextField(CommonPageElements.SEARCH.locate());
		click(OnsiteNotificationPageElements.SEARCH_BAR.locate());

	}

	private String getElementValue(String parameter) {
		String value = "";
		List<WebElement> elements = findElements(OnsiteNotificationPageElements.LAYOUT_TYPE.locate());
		for (WebElement element : elements) {
			if (element.getText().contains(parameter)) {
				String arr[] = element.getText().split(":");
				value = arr[1].trim();
			}
		}
		return value;
	}

	/**
	 * This method will click on 'Create Notification' button and will select the
	 * onsite-notifcation type.
	 */
	public void createNotification(String name) {
		click(OnsiteNotificationPageElements.CREATE_NOTIFICATION_BTN.locate());
		if ((" Create Your Own ").equalsIgnoreCase(name))
			click(OnsiteNotificationPageElements.CREATE_OWN.locate());
		else
			click(OnsiteNotificationPageElements.CREATE_GALLERY.locate());
	}

	/**
	 * This method will select the desired template type of onsite-notification.
	 */
	public void selectOnsiteTemplate(String name) {
		click(dynamicXpathLocator(OnsiteNotificationPageElements.ONSITE_TEMPLATE, name));
	}

	public void gotoList(String campaignType) {
		click(dynamicXpathLocator(OnsiteNotificationPageElements.LIST_PAGE, campaignType));
	}


	public void switchToDefaultWindow() {
		ArrayList<String> w = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		// switch to first tab
		driver.switchTo().window(w.get(0));
		Log.debug("Parent tab title: " + driver.getTitle());
		Log.debug("Parent tab url: " + getCurrentURL());
	}

	public void verifyOnsiteNotificationDetails(JSONObject template) {
		JSONObject expectedTitle1 = (JSONObject) template.get("ExpectedData");
		String title = expectedTitle1.get("Title").toString();
		CampaignsPage.setCampaignName(title);
		Log.debug("Expected title from JSON: " + title);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.debug("Waiting for SDKPageResponse..");
		comPage.waitForSDKPageResponse(OnsiteNotificationPageElements.ONSITE_NOTIFICATION_IFRAME.locate(),
				System.currentTimeMillis(), "");
		Log.debug("Element located.");
		switchToiFrame(OnsiteNotificationPageElements.ONSITE_NOTIFICATION_IFRAME.locate());
		Log.debug("Performing validation.");
		Log.debug("Actual Title from DOM: " + fetchText(OnsiteNotificationPageElements.ACTUAL_TITLE.locate()));
		CustomAssertions.assertEquals("Notification title does not match", title,
				fetchText(OnsiteNotificationPageElements.ACTUAL_TITLE.locate()));
		click(OnsiteNotificationPageElements.ONSITE_NOTIFICATION_CLOSE.locate());
		Log.debug("validation passed hence switching to parent window..");
		switchToDefaultWindow();
		Log.debug("switched to my parent window successfully");

	}

	public void deleteOnsiteNotification(JSONObject template) {
		JSONObject expectedTitle1 = (JSONObject) template.get("ExpectedData");
		String title = expectedTitle1.get("Title").toString();
		sendKeys(CommonPageElements.SEARCH.locate(), title);
		click(OnsiteNotificationPageElements.MORE_TAB.locate());
		click(OnsiteNotificationPageElements.DELETE_NOTIFICATION.locate());
		acceptAlert();
	}

}
