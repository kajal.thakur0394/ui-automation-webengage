package com.webengage.ui_automation.pages;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import com.webengage.ui_automation.elements.AnalyticsPageElements;
import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.SegmentsPageElements;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class AnalyticsPage extends SeleniumExtendedUtility {
	CommonPage commPage = new CommonPage();

	public void eventFilterSelection(List<List<String>> filterOptions) {
		waitForSpinner();
		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (List<String> selectVal : filterOptions) {
			String selectDD = selectVal.get(0);
			String selectDDvalue = selectVal.get(1);
			if (selectDDvalue.equals("-"))
				continue;
			click(dynamicXpathLocator(AnalyticsPageElements.SELECT_DD, selectDD));
			if (selectDD.equals("OF"))
				sendKeys(CommonPageElements.SEARCH.locate(), selectDDvalue);
			click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, selectDDvalue));
		}
		click(CommonPageElements.SYNC_BTN.locate());
		waitForElementToBeInvisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, "Event name"));
	}

	public void validateTableContent(List<List<String>> cellValues) {
		for (List<String> cellVal : cellValues) {
			String fieldName = cellVal.get(0);
			if (cellVal.get(0).contains("Date")) {
				fieldName = getPreviousDate();
			}
			int expectedValue = Integer.parseInt(cellVal.get(1).replaceAll("\\D+", ""));
			int actualValue = Integer
					.parseInt(fetchText(dynamicXpathLocator(CommonPageElements.TABLE_VALUE_CHECK, fieldName)));
			if (cellVal.get(1).contains(">"))
				CustomAssertions.assertTrue("Expected value condition not staisfied", actualValue > expectedValue);
		}
	}

	private String getPreviousDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		SimpleDateFormat format1 = new SimpleDateFormat("d MMM", Locale.ENGLISH);
		return format1.format(new Date(cal.getTimeInMillis()));
	}

	public void pathFilterSelection(List<List<String>> pathFilters) {
		for (List<String> selectVal : pathFilters) {
			String label = selectVal.get(0);
			for (int i = 1; i < selectVal.size(); i++) {
				try {
					if (selectVal.get(i).contains(">")) {
						String columnName = selectVal.get(i).split(">")[0];
						String columnValue = selectVal.get(i).split(">")[1];
						click(dynamicXpathLocator(SegmentsPageElements.OPEN_DD_SELECT, label, columnName));
						commPage.selectInDropdown(
								dynamicXpathLocator(SegmentsPageElements.OPEN_DD_SELECT, label, columnName),
								columnValue);
					} else if (label.contains("COLLAPSE")) {
						if (selectVal.get(i).contains("true"))
							click(dynamicXpathLocator(CommonPageElements.LABEL_CHECKBOX, label));
					} else
						clearFieldAndSendkeys(dynamicXpathLocator(CommonPageElements.LABEL_TEXT_FIELD, label),
								selectVal.get(i));
				} catch (NullPointerException e) {
					continue;
				}
			}
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Show path"));
	}

	public void validateStepinHighChart(String eventName, String stepNumber) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT_CONTAINS, eventName + " 100%"));
		waitForElementToBeVisible(dynamicXpathLocator(AnalyticsPageElements.PATH_STEP, eventName, stepNumber));
	}

	public void enterFunnelName(String funnelName) {
		sendKeys(AnalyticsPageElements.INPUT_FUNNEL.locate(), funnelName);
	}

	public void addFunnelSteps(List<List<String>> funnelStepFilters) {
		addSteps(funnelStepFilters);
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Create Funnel"));
	}

	private void addSteps(List<List<String>> funnelStepFilters) {
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.BUTTON, "Add Step"));
		funnelStepFilters.forEach(s -> {
			String stepNo = s.get(0);
			String eventName = s.get(1);
			if (!checkIfElementisLocated(dynamicXpathLocator(AnalyticsPageElements.FUNNEL_STEP_DD, stepNo))) {
				click(dynamicXpathLocator(CommonPageElements.BUTTON, "Add Step"));
			} 
			click(dynamicXpathLocator(AnalyticsPageElements.FUNNEL_STEP_DD, stepNo));
			sendKeys(CommonPageElements.SEARCH.locate(), eventName);
			click(dynamicXpathLocator(AnalyticsPageElements.SELECT_FUNNEL_EVENT, eventName));
		});
	}
}