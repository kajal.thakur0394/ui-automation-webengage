package com.webengage.ui_automation.pages;

import java.util.List;

import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.RecommendationsPageElements;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class RecommendationsPage extends SeleniumExtendedUtility {

	public void fillRecommendationsDetails(List<List<String>> recDetails) {
		for (List<String> choice : recDetails) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			switch (parameter) {
			case "Strategy Name":
				sendKeys(dynamicXpathLocator(RecommendationsPageElements.INPUT_DETAIL, parameter), value);
				break;
			case "Catalog":
				selectDataFromDropdown(parameter, value);
				break;
			case "Type":
				selectDataFromDropdown(parameter, value);
				break;
			case "Recommendation Event":
				selectDataFromDropdown(parameter, value);
				break;
			}
		}
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "Create Recommendation"));
	}

	private void selectDataFromDropdown(String parameter, String value) {
		click(dynamicXpathLocator(RecommendationsPageElements.PLACEHOLDER_DD, parameter));
		sendKeys(dynamicXpathLocator(RecommendationsPageElements.SELECT_INPUT, parameter), value);
		click(dynamicXpathLocator(CommonPageElements.CATALOG_TYPE_VALUE, value));
	}

	public void validateRecommendationPresent(String recName, String catalogName) {
		CustomAssertions.assertEquals("Catalog name does not match", catalogName,
				fetchAttributeValue(dynamicXpathLocator(RecommendationsPageElements.CATALOG_EVENTLINK,recName), "title"));
		CustomAssertions.assertEquals("Strategy name does not match", recName,
				fetchText(RecommendationsPageElements.STRATEGY_NAME.locate()));
	}

}
