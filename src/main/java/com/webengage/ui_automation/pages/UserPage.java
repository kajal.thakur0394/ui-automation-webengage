package com.webengage.ui_automation.pages;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.UserPageElement;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class UserPage extends SeleniumExtendedUtility {

	public void selectUser(String userId) {
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search by email, user ID, phone"),
				userId + Keys.RETURN);
		click(dynamicXpathLocator(UserPageElement.CLICK_USER, userId));
		Log.info("Selected user id " + userId);
	}

	public void verifyDetails(List<List<String>> labels) {
		for (List<String> label : labels) {
			String attribute = label.get(0);
			String expectedValue = label.get(1);
			String actualValue = fetchText(dynamicXpathLocator(UserPageElement.LABEL_VALUE, attribute));
			CustomAssertions.assertEquals("Value for " + attribute + " does not match", expectedValue, actualValue);
		}
	}

	public void verifyRechability(List<List<String>> labels) {
		for (List<String> label : labels) {
			String expectedChannel = label.get(0).toLowerCase();
			String expectedChannelReachability = label.get(1);
			int i = 1;
			for (WebElement el : findElements(UserPageElement.REACHABILITY_HEADER.locate())) {
				if (el.getText().toLowerCase().equals(expectedChannel.toLowerCase())) {
					CustomAssertions.assertTrue("Reachability attribute doesn't match",
							fetchAttributeValue(
									dynamicXpathLocator(UserPageElement.REACHABILITY_VALUE, String.valueOf(i)), "class")
							.contains(expectedChannelReachability));
					break;
				}
				++i;
			}
		}
	}

	public void openSection(String section) {
		click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, section));
	}

	public void openDevice(String deviceNo) {
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, deviceNo));
	}

	public void checkEvents(String period) {
		waitForElementToBeVisible(UserPageElement.EVENT_PAGE.locate());
		click(CommonPageElements.TIME_PERIOD.locate());
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, period));
		waitForElementToBeVisible(dynamicXpathLocator(CommonPageElements.BUTTON, "Load More"));
		CustomAssertions.assertTrue("Event list is empty for defined period as " + period,
				findElements(UserPageElement.USER_EVENT_LIST.locate()).size() > 0);
	}

	public void verifyTotalUsers(List<List<String>> cellValues) {
		for (List<String> cellVal : cellValues) {
			int expectedValue = Integer.parseInt(cellVal.get(1).replaceAll("\\D+", ""));
			int actualValue = Integer.parseInt(
					fetchText(dynamicXpathLocator(UserPageElement.CARD_STAT, cellVal.get(0))).replaceAll("\\D+", ""));
			if (cellVal.get(1).contains(">"))
				CustomAssertions.assertTrue("Expected value condition not staisfied", actualValue > expectedValue);
			else
				CustomAssertions.assertEquals("Values don't match", expectedValue, actualValue);
		}
	}

	public void downloadListOfUsers() {
		click(CommonPageElements.DOWNLOAD_BTN.locate());
		new CommonPage().downloadListOfUsers();
	}
}
