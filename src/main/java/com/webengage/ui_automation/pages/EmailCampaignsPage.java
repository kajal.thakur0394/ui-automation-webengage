package com.webengage.ui_automation.pages;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.EmailCampaignPageElements;
import com.webengage.ui_automation.elements.SMSCampaignPageElements;
import com.webengage.ui_automation.utils.CustomAssertions;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class EmailCampaignsPage extends SeleniumExtendedUtility {
	private String expectedEmailText;
	CommonPage comPage = new CommonPage();
	public void fillEmailDetails(List<List<String>> choices) throws InterruptedException {
		for (List<String> choice : choices) {
			String parameter = choice.get(0);
			String value = choice.get(1);
			if (("From name").equalsIgnoreCase(parameter)) {
				sendKeys(dynamicXpathLocator(EmailCampaignPageElements.emailInputs, parameter), value);
			} else if (("From email").equalsIgnoreCase(parameter)) {
				sendKeys(dynamicXpathLocator(EmailCampaignPageElements.emailInputs, parameter), value);
			} else if (("Subject").equalsIgnoreCase(parameter)) {
				sendKeys(dynamicXpathLocator(EmailCampaignPageElements.emailInputs, parameter), value);
			} else if (("Body").equalsIgnoreCase(parameter)) {
				String[] arr = value.split(">");
				expectedEmailText = arr[1];
				if (arr[0].equalsIgnoreCase("Rich Text")) {
					clearFieldAndSendkeys(EmailCampaignPageElements.IFRAME.locate(), expectedEmailText);
					saveAndContinueButton();
					 try {
				            // This try-catch block is to handle Transactional campaign. Sine no 'proceed'
				            // modal appears.
				            waitForElementToBeVisible(EmailCampaignPageElements.PROCEED_BTN.locate());
				            click(EmailCampaignPageElements.PROCEED_BTN.locate());
				        } catch (Exception e) {
				            e.printStackTrace();
				        }

				} else if (arr[0].equalsIgnoreCase("Drag & Drop Editor ")) {
					click(EmailCampaignPageElements.TEMPLATE.locate());
					click(EmailCampaignPageElements.BASIC_TEMPLATE.locate());
					click(EmailCampaignPageElements.BLANK_TYPE.locate());
					waitForElementToBeClickable(CommonPageElements.SAVE_AND_CLOSE_BUTTON.locate());
					switchToiFrame(EmailCampaignPageElements.TEMP_IFRAME.locate());
					waitForElementToBeVisible(EmailCampaignPageElements.CONTENT.locate());
					dragAndDropElement(EmailCampaignPageElements.DRAG.locate(),
							EmailCampaignPageElements.DROP.locate());
					waitForElementToBeClickable(EmailCampaignPageElements.TEXT.locate());
					Thread.sleep(2000);
					clearTextField(EmailCampaignPageElements.TEXT.locate());
					Thread.sleep(1000);
					sendKeys(EmailCampaignPageElements.TEXT.locate(), expectedEmailText);
					switchToDefaultContent();
					click(CommonPageElements.SAVE_AND_CLOSE_BUTTON.locate());
					waitForElementToBeInvisible(CommonPageElements.SAVE_AND_CLOSE_BUTTON.locate());
					waitForElementToBeClickable(CommonPageElements.SAVE_AND_CONTINUE_BUTTON.locate());
					saveAndContinueButton();
					waitForElementToBeVisible(EmailCampaignPageElements.PROCEED_BTN.locate());
					click(EmailCampaignPageElements.PROCEED_BTN.locate());
				}
			}
		}
	}

	public void verifyAttachedTemplate(String status,String campaignName) {
		comPage.openCampaign(campaignName);
		if(status.toLowerCase().contains("running"))
			click(EmailCampaignPageElements.EDIT.locate());
		waitForElementToBeClickable(SMSCampaignPageElements.PREVIEWPAGE.locate());
		click(SMSCampaignPageElements.PREVIEWPAGE.locate());
		waitForElementToBePresent(EmailCampaignPageElements.VERIFY_IFRAME.locate());
		switchToiFrame(EmailCampaignPageElements.VERIFY_IFRAME.locate());
		CustomAssertions.assertEquals("Email Template Text do not match:", fetchText(EmailCampaignPageElements.PREVIEW_EMAIL_CONTENT.locate()), expectedEmailText);
		switchToDefaultContent();
	}

	public void verifyAttachedJourneyCampaign(String expectedStatus, String journeyName) {
		CustomAssertions.assertEquals("Strings do not match:", "Journey", fetchText(dynamicXpathLocator(SMSCampaignPageElements.CAMPAIGNTYPE_TEXT, "Journey")));
		String actualStatus=fetchText(SMSCampaignPageElements.CAMPAIGN_STATUS.locate());
		CustomAssertions.assertEquals("Status do not match:", expectedStatus.toLowerCase(), actualStatus.toLowerCase());
		/* @issue:WP-9451  
		String expectedDate=JourneysPage.journeys.get(journeyName).get("Start Date");
		String actualDate=fetchText(SMSCampaignPageElements.CAMPAIGN_START_DATE.locate());
		CustomAssertions.assertEquals("Dates do not match:", expectedDate, actualDate);*/
	}

	public void dragDropTextField(HashMap<String, String> map) throws InterruptedException {
		String textToSend = map.get("ValueToSend");
		switchToiFrame(EmailCampaignPageElements.TEMP_IFRAME.locate());
		waitForElementToBeVisible(EmailCampaignPageElements.CONTENT.locate());
		dragAndDropElement(EmailCampaignPageElements.DRAG.locate(),
				EmailCampaignPageElements.DROP.locate());
		waitForElementToBeClickable(EmailCampaignPageElements.TEXT.locate());
		TimeUnit.SECONDS.sleep(2);
		clearTextField(EmailCampaignPageElements.TEXT.locate());
		TimeUnit.SECONDS.sleep(1);
		sendKeys(EmailCampaignPageElements.TEXT.locate(),textToSend);
		switchToDefaultContent();
	}

}
