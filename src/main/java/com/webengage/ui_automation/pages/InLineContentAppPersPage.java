package com.webengage.ui_automation.pages;

import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.InlineContentAppPerPageElements;
import com.webengage.ui_automation.elements.InlineContentWebPerPageElements;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;


public class InLineContentAppPersPage extends SeleniumExtendedUtility {

	public void selectProperty(String property) {
		openDropdown(InlineContentWebPerPageElements.PROP_DROPDOWN.locate());
		sendKeys(dynamicXpathLocator(CommonPageElements.GENERIC_PLACEHOLDER, "Search"), property);
		click(dynamicXpathLocator(CommonPageElements.GENERIC_TEXT, property));
	}

	public void selectLayout(String layoutType) {
		click(dynamicXpathLocator(InlineContentAppPerPageElements.LAYOUT, layoutType));
	}
}
