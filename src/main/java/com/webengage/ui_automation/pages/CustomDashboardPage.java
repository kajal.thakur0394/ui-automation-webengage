package com.webengage.ui_automation.pages;

import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.CustomDashboardPageElements;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;

public class CustomDashboardPage extends SeleniumExtendedUtility {
	CommonPage commonPage = new CommonPage();

	public void enterDashboardName(String dashboardName) {
		sendKeys(CustomDashboardPageElements.DASHBOARD_INPUT.locate(), dashboardName);
	}

	public void selectDashboardVisibility(String visibilityType) {
		click(CustomDashboardPageElements.VISIBILITY_DROPDOWN.locate());
		click(dynamicXpathLocator(CommonPageElements.CATALOG_TYPE_VALUE, visibilityType));
	}

	public void createDashboard() {
		click(dynamicXpathLocator(CommonPageElements.BUTTON, "create Dashboard"));
	}
}
