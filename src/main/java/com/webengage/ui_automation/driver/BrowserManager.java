package com.webengage.ui_automation.driver;

import java.io.File;
import java.util.HashMap;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.safari.SafariDriver;
import net.thucydides.core.webdriver.ThucydidesWebDriverSupport;

public class BrowserManager extends ThucydidesWebDriverSupport {
	public WebDriver driver = null;
	public static String downloadDir = System.getProperty("user.dir") + "/src/test/resources/downloads";
	public String osName = System.getProperty("os.name");
	public String osArch = System.getProperty("os.arch");

	public BrowserManager() {
		String browser = System.getProperty("set.Browser");
		switch (browser) {
		case "chrome":
			DriverFactory.getInstance().setDriver(invokeChrome());
			driver = DriverFactory.getInstance().getDriver();
			break;
		case "firefox":
			DriverFactory.getInstance().setDriver(invokeFirefox());
			driver = DriverFactory.getInstance().getDriver();
			break;
		case "safari":
			DriverFactory.getInstance().setDriver(invokeSafari());
			driver = DriverFactory.getInstance().getDriver();
			break;
		case "chrome-headless":
			DriverFactory.getInstance().setDriver(invokeChromeHeadless());
			driver = DriverFactory.getInstance().getDriver();
			break;
		default:
			break;
		}
		driver.manage().window().maximize();
	}

	private WebDriver invokeSafari() {
		WebDriver driver = new SafariDriver();
		useDriver(driver);
		return getDriver();
	}

	private WebDriver invokeFirefox() {
		FirefoxOptions option = buildFirefoxProfile();
		WebDriver driver = new FirefoxDriver(option);
		useDriver(driver);
		return getDriver();
	}

	private WebDriver invokeChrome() {
		ChromeOptions options = buildChromeProfile();
		WebDriver driver = new ChromeDriver(options);
		useDriver(driver);
		return driver;
	}

	private WebDriver invokeChromeHeadless() {
		ChromeOptions options = buildChromeProfile();
		options.addArguments("--headless=new");
		options.addArguments("--window-size=1920,1080");
		WebDriver driver = new ChromeDriver(options);
		useDriver(driver);
		return driver;
	}

	private ChromeOptions buildChromeProfile() {
		ChromeOptions options = new ChromeOptions();
		HashMap<String, Object> preferences = new HashMap<>();
		preferences.put("profile.default_content_settings.popups", 0);
		preferences.put("safebrowsing.enabled", true);
		preferences.put("download.prompt_for_download", false);
		preferences.put("directory_upgrade", true);
		preferences.put("download.default_directory", downloadDir.replace("/", File.separator));
		preferences.put("profile.content_settings.exceptions.automatic_downloads.*.setting", 1);
		options.setExperimentalOption("prefs", preferences);
		options.addArguments("--disable-extensions");
		options.addArguments("--remote-allow-origins=*");
		return options;
	}

	private FirefoxOptions buildFirefoxProfile() {
		FirefoxOptions option = new FirefoxOptions();
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.dir", downloadDir.replace("/", File.separator));
		profile.setPreference("privacy.popups.showBrowserMessage", true);
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "image/png,image/jpeg");
		option.setProfile(profile);
		return option;
	}
}
