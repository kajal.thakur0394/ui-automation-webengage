package com.webengage.ui_automation.driver;

import org.openqa.selenium.WebDriver;

public class DriverFactory {
	private static DriverFactory instance;
	private static ThreadLocal<WebDriver> driverLocal = new ThreadLocal<WebDriver>();

	private DriverFactory() {
	}

	public static DriverFactory getInstance() {
		if (instance == null) {
			instance = new DriverFactory();
		}
		return instance;
	}

	public void setDriver(WebDriver driver) {
		driverLocal.set(driver);
	}

	public WebDriver getDriver() {
		return driverLocal.get();

	}

	public void closeBrowser() {
		driverLocal.get().close();
		driverLocal.remove();
	}
}
