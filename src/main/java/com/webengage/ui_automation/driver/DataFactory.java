package com.webengage.ui_automation.driver;

import java.util.HashMap;

import com.google.common.primitives.Primitives;


public class DataFactory {
	private static DataFactory instance;
	private static ThreadLocal<HashMap<String, Object>> dataLocal = new ThreadLocal<HashMap<String, Object>>();

	private DataFactory() {
	}

	public static DataFactory getInstance() {
		if (instance == null) {
            instance = new DataFactory();
        }
        return instance;
    }
		
	public void setData(String variableName, Object object) {
		if (dataLocal.get() == null) {
			dataLocal.set(new HashMap<>());
		}
		dataLocal.get().put(variableName, object);
		
	}

	public <T> T getData(String variableName, Class<T>  className) {
		return Primitives.wrap(className).cast(dataLocal.get().get(variableName));
	}
	
	
	public void clearData() {
		dataLocal.get().clear();
		dataLocal.remove();
	}
	
}
