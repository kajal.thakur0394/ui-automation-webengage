package com.webengage.ui_automation.utils;

import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class Log {

	// Initialize Log4j instance
	private static final Logger log = LogManager.getLogger(Log.class);

	// Info Level Logs
	public static void info(String message) {
		setThreadContext();
		log.info(message);
		ThreadContext.clearAll();
	}

	// Warn Level Logs
	public static void warn(String message) {
		setThreadContext();
		log.error(message);
		ThreadContext.clearAll();
	}

	// Error Level Logs
	public static void error(String message) {
		setThreadContext();
		log.error(message);
		ThreadContext.clearAll();
	}

	// Fatal Level Logs
	public static void fatal(String message) {
		setThreadContext();
		log.error(message);
		ThreadContext.clearAll();
	}

	// Debug Level Logs
	public static void debug(String message) {
		setThreadContext();
		log.error(message);
		ThreadContext.clearAll();
	}

	private static void setThreadContext() {
		if (Objects.isNull(System.getProperty("featureFileName")))
			ThreadContext.put("featureFileName", "dashboard-automation");
		else
			ThreadContext.put("featureFileName", System.getProperty("featureFileName"));
		ThreadContext.push("Build#" + System.getProperty("set.BuildNumber"));
	}

}
