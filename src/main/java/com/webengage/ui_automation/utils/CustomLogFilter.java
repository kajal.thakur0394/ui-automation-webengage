package com.webengage.ui_automation.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.webengage.ui_automation.driver.DataFactory;

import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;

public class CustomLogFilter implements Filter {

	public static String getLogRequest() {
		return DataFactory.getInstance().getData("logRequest", String.class);
	}

	public static void setLogRequest(String logRequest) {
		DataFactory.getInstance().setData("logRequest", logRequest);
	}

	public static String getLogResponse() {
		return DataFactory.getInstance().getData("logResponse", String.class);
	}

	public static void setLogResponse(String logResponse) {
		DataFactory.getInstance().setData("logResponse", logResponse);
	}

	/**
	 * 
	 * @param jsonString
	 * @return prettyJson
	 */

	public String toPrettyFormat(String jsonString) {
		String prettyJson = null;
		try {
			JsonObject jsonObj = JsonParser.parseString(jsonString).getAsJsonObject();
			Gson gsonObj = new GsonBuilder().setPrettyPrinting().create();
			prettyJson = gsonObj.toJson(jsonObj);
		} catch (IllegalStateException e) {
			JsonArray jsonArr = JsonParser.parseString(jsonString).getAsJsonArray();
			Gson gsonObj = new GsonBuilder().setPrettyPrinting().create();
			prettyJson = gsonObj.toJson(jsonArr);
		}
		return prettyJson;
	}

	@Override
	public Response filter(FilterableRequestSpecification requestSpec, FilterableResponseSpecification responseSpec,
			FilterContext filterCtx) {
		Response response = filterCtx.next(requestSpec, responseSpec);

		StringBuilder requestBuilder = new StringBuilder();
		requestBuilder.append("Request method: " + requestSpec.getMethod() + "\n");
		requestBuilder.append("Request URI: " + requestSpec.getURI() + "\n");
		requestBuilder.append("Query params: \n" + requestSpec.getQueryParams().toString() + "\n");
		requestBuilder.append("Headers:\n" + requestSpec.getHeaders().toString() + "\n");
		try {
			requestBuilder.append("Body: \n" + toPrettyFormat(requestSpec.getBody().toString()));
		} catch (NullPointerException e) {
		}
		String logRequest = requestBuilder.toString().replace("%7E", "~");
		setLogRequest(logRequest);

		StringBuilder responseBuilder = new StringBuilder();
		responseBuilder.append("Status Code: " + response.getStatusCode() + "\n");
		responseBuilder.append("Response: \n" + response.body().asPrettyString());
		String logResponse = responseBuilder.toString().replace("%7E", "~");
		setLogResponse(logResponse);

		return response;
	}
}
