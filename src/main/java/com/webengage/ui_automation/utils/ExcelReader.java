package com.webengage.ui_automation.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
	public static String testDataexcel = SetupUtility.runtimePayloadsDirectory + "testData.xlsx";
	public static String resultSummaryWB = SetupUtility.directoryLoc + "resultSummary.xlsx";
	public static int rowNum;
	public static XSSFWorkbook resultWorkbook;
	public static XSSFSheet resultSummarySheet;

	@SuppressWarnings("resource")
	public static XSSFSheet readFromExcelSheet(String sheetName) throws IOException {
		FileInputStream fis = new FileInputStream(testDataexcel);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet(sheetName);
		return sheet;
	}

	public LinkedList<HashMap<String, HashMap<String, String>>> processExcelSheetRowData(XSSFSheet sheet)
			throws Exception {
		LinkedList<HashMap<String, HashMap<String, String>>> userList = new LinkedList<>();
		Iterator<Row> rows = sheet.rowIterator();
		String temp = "";
		Row headerRow = rows.next();
		while (rows.hasNext()) {
			HashMap<String, HashMap<String, String>> keyMap = new HashMap<>();
			HashMap<String, String> valueMap = new HashMap<>();
			Iterator<Cell> cells = rows.next().cellIterator();
			Iterator<Cell> headerCell = headerRow.cellIterator();
			while (cells.hasNext()) {
				String headerCellContent = headerCell.next().getStringCellValue().toString();
				String className = headerCellContent.split(":")[0];
				String method;
				try {
					method = headerCellContent.split(":")[1] + "-" + headerCellContent.split(":")[2];
				} catch (ArrayIndexOutOfBoundsException e) {
					method = headerCellContent.split(":")[1] + "-String";
				}
				Cell value = cells.next();
				String cellValue;
				try {
					cellValue = value.getStringCellValue().replaceAll("\"", "");
				} catch (Exception e) {
					cellValue = String.valueOf(value.getNumericCellValue());
				}
				if (temp.isEmpty()) {
					valueMap.put(method, cellValue);
					keyMap.put(className, valueMap);
				} else if (temp.equals(className)) {
					valueMap.put(method, cellValue);
					keyMap.put(className, valueMap);
				} else {
					valueMap = new HashMap<>();
					valueMap.put(method, cellValue);
					keyMap.put(className, valueMap);
				}
				temp = className;
			}
			userList.add(keyMap);
		}
		return userList;
	}

	public HashMap<String, String> processExcelSheetData(XSSFSheet sheetObj, String refId) {
		Iterator<Row> rows = sheetObj.rowIterator();
		HashMap<String, String> postReq = new HashMap<>();
		Row headerRow = rows.next();
		while (rows.hasNext()) {
			Iterator<Cell> cells = rows.next().cellIterator();
			Cell identifier = cells.next();
			if (identifier.getStringCellValue().equals(refId)) {
				Iterator<Cell> headerCell = headerRow.cellIterator();
				headerCell.next();
				while (cells.hasNext()) {
					if (headerCell.next().getStringCellValue().equals("QueryParams")) {
						postReq.put("QueryParams", cells.next().getStringCellValue());
					} else {
						postReq.put("Body", cells.next().getStringCellValue());
					}
				}
				break;
			}
		}
		return postReq;
	}

	public static void initializeTestResultsSheet(LinkedList<LinkedHashMap<String, String>> scenarioDetails)
			throws IOException {
		FileInputStream fis = new FileInputStream(resultSummaryWB);
		resultWorkbook = new XSSFWorkbook(fis);
		for (int i = 0; i < resultWorkbook.getNumberOfSheets(); i++) {
			resultWorkbook.removeSheetAt(i);
		}
		resultSummarySheet = resultWorkbook.createSheet("resultMetrics");
		rowNum = 0;
		Row currentRow = resultSummarySheet.createRow(ExcelReader.rowNum++);
		CellStyle cs = resultWorkbook.createCellStyle();
		cs.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		cs.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Cell buildDetails = currentRow.createCell(0);
		buildDetails.setCellStyle(cs);
		buildDetails.setCellValue("Build Details");
		Cell urlCell = currentRow.createCell(1);
		urlCell.setCellStyle(cs);
		urlCell.setCellValue("https://jenkins.stg.webengage.biz/job/UI%20Automation/"
				+ System.getProperty("set.BuildNumber") + "/parameters/");
		ExcelReader.rowNum++;
		int cellNo = 0;
		currentRow = resultSummarySheet.createRow(ExcelReader.rowNum++);
		for (Entry<String, String> e : scenarioDetails.get(0).entrySet()) {
			currentRow.createCell(cellNo).setCellValue(e.getKey());
			cellNo++;
		}
		for (LinkedHashMap<String, String> scenario : scenarioDetails) {
			currentRow = resultSummarySheet.createRow(ExcelReader.rowNum++);
			cellNo = 0;
			for (Entry<String, String> e : scenario.entrySet()) {
				currentRow.createCell(cellNo).setCellValue(e.getValue());
				cellNo++;
			}
		}
		writeWB();
	}

	public static void writeWB() throws IOException {
		FileOutputStream outputStream = new FileOutputStream(resultSummaryWB);
		resultWorkbook.write(outputStream);
		resultWorkbook.close();
		outputStream.close();
	}

	public LinkedList<HashMap<String, String>> filterRowsOnFunctions(XSSFSheet sheetObj, String function) {
		LinkedList<HashMap<String, String>> data = new LinkedList<>();
		Iterator<Row> rows = sheetObj.rowIterator();
		Row headerRow = rows.next();
		while (rows.hasNext()) {
			Iterator<Cell> cells = rows.next().cellIterator();
			Cell identifier = cells.next();
			LinkedHashMap<String, String> values = new LinkedHashMap<>();
			if (identifier.getStringCellValue().equals(function)) {
				Iterator<Cell> headerCell = headerRow.cellIterator();
				headerCell.next();
				while (cells.hasNext()) {
					values.put(headerCell.next().getStringCellValue(), cells.next().getStringCellValue().trim());
				}
				data.add(values);
			}
		}
		return data;
	}
}
