package com.webengage.ui_automation.initialize;

import com.google.common.io.Files;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.SetupUtility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

import org.apache.commons.io.FileUtils;

public class RemoteConnection {
	private static String REMOTE_HOST = "10.30.2.141";
	private static int REMOTE_PORT = 22;
	private static String USERNAME = "sftpuser";
	private static String PASSWORD = "usersftp@123";
	private static final int SESSION_TIMEOUT = 10000;
	private static final int CHANNEL_TIMEOUT = 5000;
	public static String directoryLoc = System.getProperty("user.dir") + "/src/test/resources/testData/runtimeFiles/";

	public static ChannelSftp setupJsch() throws JSchException {
		JSch jsch = new JSch();
		jsch.setKnownHosts(SetupUtility.directoryLoc + "known_hosts");
		Session jschSession = jsch.getSession(USERNAME, REMOTE_HOST, REMOTE_PORT);
		jschSession.setPassword(PASSWORD);
		jschSession.connect(SESSION_TIMEOUT);
		return (ChannelSftp) jschSession.openChannel("sftp");
	}

	public static void exportCampaignPayloads(boolean localOrRemote) {
		if (localOrRemote) {
			writeFilesToLocal();
		} else
			writeFilesToRemote();
	}

	public static void writeFilesToLocal() {
		String localDir = System.getProperty("user.home") + "/Documents/campaignPayloads";
		File directory = new File(localDir);
		if (!directory.exists())
			directory.mkdir();
		try {
			FileUtils.cleanDirectory(directory);
		} catch (IOException e) {
			e.printStackTrace();
		}
		copyFilesAcrossFolder(localDir, true, null);
	}

	private static void writeFilesToRemote() {
		ChannelSftp channelSftp = null;
		String remoteDir = "/campaignPayloads";
		try {
			channelSftp = setupJsch();
			channelSftp.connect(CHANNEL_TIMEOUT);
			emptyAndDeleteDirectory(channelSftp, remoteDir + "/");
		} catch (JSchException e) {
			e.printStackTrace();
		}
		try {
			channelSftp.stat(channelSftp.pwd() + remoteDir);
		} catch (SftpException e1) {
			Log.info("Creating Directory");
			try {
				channelSftp.mkdir(remoteDir);
			} catch (SftpException e) {
				e.printStackTrace();
			}
		}
		copyFilesAcrossFolder(remoteDir, false, channelSftp);
		channelSftp.exit();
	}

	private static void copyFilesAcrossFolder(String definedDir, boolean localDirectory, ChannelSftp channelSftp) {
		for (String fileName : new File(SetupUtility.expectedPayloadsDirectory).list()) {
			if (!fileName.contains("gitkeep")) {
				String sourceDestination = SetupUtility.expectedPayloadsDirectory + fileName;
				if (localDirectory) {
					Log.info("Exporting " + fileName + " to local directory");
					Path src = Paths.get(sourceDestination);
					Path dest = Paths.get(definedDir + "/" + fileName);
					try {
						Files.copy(src.toFile(), dest.toFile());
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					Log.info("Exporting " + fileName + " to remote directory");
					try {
						channelSftp.put(SetupUtility.expectedPayloadsDirectory + fileName, definedDir + "/" + fileName);
					} catch (SftpException e) {
						e.printStackTrace();
					}
				}

			}
		}
	}

	private static void emptyAndDeleteDirectory(ChannelSftp channelSftp, String remoteDir) {
		Collection<ChannelSftp.LsEntry> fileAndFolderList = null;
		try {
			fileAndFolderList = channelSftp.ls(remoteDir);
			fileAndFolderList.forEach(s -> {
				try {
					channelSftp.rm(remoteDir + s.getFilename());
				} catch (SftpException e) {
					Log.warn("Failed to delete file");
				}
			});
			channelSftp.rmdir(remoteDir);
		} catch (SftpException e1) {
			Log.info("Directory deletion step skipped");
			e1.printStackTrace();
		}

	}

	public static FileReader readFilefromRemote(String directory, String fileName) throws FileNotFoundException {
		String localLocation = directoryLoc + fileName;
		ChannelSftp channelSftp = null;
		try {
			channelSftp = setupJsch();
			channelSftp.connect(CHANNEL_TIMEOUT);
		} catch (JSchException e) {
			Log.error("jsch connection issue");
		}
		try {
			channelSftp.get("/" + directory + "/" + fileName, localLocation);
		} catch (SftpException e) {
			Log.error("File doesn't exist");
		}
		channelSftp.exit();
		return new FileReader(localLocation);
	}

}
