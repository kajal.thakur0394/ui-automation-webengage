package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum CustomDashboardPageElements {
	DASHBOARD_INPUT(getXpath("//label[text()='Dashboard Name']/parent::div//following-sibling::*//input")),
	VISIBILITY_DROPDOWN(getXpath("//div[@class='Select-value']"));
	
	By locator;

	CustomDashboardPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

	static By getId(String element) {
		return By.id(element);
	}
}
