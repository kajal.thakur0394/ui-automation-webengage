package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum RecommendationsPageElements {
	STRATEGY_NAME(getXpath("//*[contains(@class,'table__container-fixed')]//span"));
	
	public static String INPUT_DETAIL = "//label[text()='%s']/parent::div//following-sibling::*//input";
	public static String PLACEHOLDER_DD ="//label[text()='%s']/parent::div/following-sibling::div//div[@class='Select-placeholder']";
	public static String SELECT_INPUT="//label[text()='%s']/parent::div/following-sibling::div//div[@class='Select-placeholder']/following-sibling::*/input";
	public static String CATALOG_EVENTLINK="//span[@title='%s']/ancestor::td/following-sibling::*//a";

	By locator;

	RecommendationsPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getName(String element) {
		return By.name(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
}
