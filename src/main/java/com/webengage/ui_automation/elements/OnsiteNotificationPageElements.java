package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum OnsiteNotificationPageElements {

	PERSONALIZATION_TAB_LIST(getXpath("//span[text()='Web Personalization']/ancestor::li")),
	TITLE(getXpath("//input[@name='title']")), 
	PERSONALIZATION_TAB(getXpath("//span[text()='Web Personalization']")),
	CREATE_NOTIFICATION_BTN(getXpath("//a[@class='btn-lg btn-primary']")),
	CREATE_OWN(getXpath("//div[contains(@class, 'we-create-header')]/a[1]")),
	CREATE_GALLERY(getXpath("//div[contains(@class, 'we-create-header')]/a[2])")),
	DESC_IFRAME(getXpath("//iframe[@class='redactor_helpTextEnabled']")),
	DESCRIPTION(getXpath("//html//body[@contenteditable='true']")), 
	NEW_TAB(getXpath("//div[@id='uniform-atnw']/..")),
	NOTIFICATIONS_LIST(getXpath("//div[contains(@class,'row page-header')]//a[contains(@href,'notifications/all')]")),
	NOTIFICATION_NAME(getXpath("//div[@class='row list-item-header']")),
	ACTUAL_TITLE(getXpath("//div[@class='title-container']/span")),
	ONSITE_NOTIFICATION_CLOSE(getXpath("//a[@class='close']")),
	ONSITE_NOTIFICATION_IFRAME(getXpath("//iframe[contains(@name,'notification')]")),
	MORE_TAB(getXpath("//i[@class='weicon we_menu']")),
	DELETE_NOTIFICATION(getXpath("//a[contains(@class,'delete')]")),
	DEACTIVATE_NOTIFICATION(getXpath("//a[contains(@class,'change-status active')]")),
	SEARCH_BAR(getXpath("//span[contains(@class,'sb-icon-search')]")),
	NAME_TEXT(getXpath("//div[contains(@class,'list-item-container')]//div[contains(@class,'list-item-header-left')]")), 
	LAYOUT_TYPE(getXpath("//div[@class='list-item-footer-tag']")),
	ONSITE_JOURNEY_IFRAME(getXpath("//div[contains(@class,'modal--journey')]//following-sibling::iframe"));

	public static String SAVE="//label[text()='%s']";
	public static String PERSONALIZATION_TYPE = "//span[text()='%s']";
	public static String ONSITE_TEMPLATE = "//div[text()='%s']";
	public static String TAB = "//div[@id='%s']/..";
	public static String NEW_TITLE = "//input[@name='%s']";
	public static String LIST_PAGE="//div[contains(@class,'row page-header')]//a[contains(@href,'%s/all')]";
	By locator;

	OnsiteNotificationPageElements(By locator) {
		this.locator = locator;
	}

	

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
}
