package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum WebPushCampaignsPageElements {
    
	TITLE(getXpath("//label[text()='Title']/../following-sibling::div//input")),
	MESSAGE(getXpath("//label[text()='Message']/../following-sibling::div//textarea")),
	ON_CLICK_ACTION(getXpath("//*[text()='Basic']/ancestor::div[@class='card']//input[@placeholder='https://www.abc.com']")),
	BUTTON_TOGGLE(getXpath("//*[text()='Buttons ']/following-sibling::div//div[@class='react-toggle']")),
	AUTO_HIDE(getXpath("//input[@value='requireInteraction']/..")),
	BADGE_CUSTOM_IMAGE(getXpath("//h5[text()='Advanced Options']/ancestor::div[@class='card']/child::div//input[@type='text']")),
	BADGE_CUSTOM_IMAGE_UPLOAD(getXpath("//h5[text()='Advanced Options']/ancestor::div[@class='card']/child::div//input[@type='file']")),
	BUTTON_LABEL(getXpath("//label[text()='Label']/../following-sibling::div//input")),
	BUTTON_ON_CLICK_ACTION(getXpath("//div[contains(@class,'row')]//div[2]//div[2]//div[2]//input")),
	IMAGE_INPUT(getXpath("//label[text()='Image']/../following-sibling::div//input[@type='text']")),
	PREVIEW_TITLE(getXpath("//div[@class='web-push-preview__title']")),
	PREVIEW_MESSAGE(getXpath("//div[@class='web-push-preview__description we__win10__description']")),
	PREVIEW_BUTTON_1(getXpath("//div[@class='we__win10__button']")),
	PREVIEW_BUTTON_2(getXpath("//div[@class='col-12 we__win10__button']"));

	public static String IMAGE_OR_ICON = "//label[text()='%s']/../following-sibling::div//input[@placeholder='https://www.abc.com/image.png']";
	public static String BUTTON_FIELDS = "//h6[text()='%s1']/../following-sibling::div//*[text()='%s2']/../following-sibling::div//input[@type='text']";
	public static String BADGE = "//input[@value='%s']/..";
	public static String IMAGE_OR_ICON_UPLOAD = "//label[text()='%s']/../following-sibling::div//label[text()='Upload']/input";

	By locator;

	WebPushCampaignsPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

}
