package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum AnalyticsPageElements {
	INPUT_FUNNEL(getXpath("//input[@type='text']"));

	public static String SELECT_DD = "//label[text()='%s']/following-sibling::div";
	public static String PATH_STEP = "//*[contains(@class,'highcharts-point') and contains(@class,'%s1__*we*__%s2')]";
	public static String SELECT_FUNNEL_EVENT = "//div[@class='r-ss-dropdown-options']//div[@title='%s']";
	public static String FUNNEL_STEP_DD = "//label[normalize-space(.)='%s']/parent::div/following-sibling::div//div[@role='combobox']";
	
	By locator;

	AnalyticsPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getName(String element) {
		return By.name(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.xpath(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}
}
