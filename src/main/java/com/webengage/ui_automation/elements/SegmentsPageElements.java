package com.webengage.ui_automation.elements;

import org.openqa.selenium.By;

public enum SegmentsPageElements {
	
	TAG(getXpath("//span[@class='tag-pill']")),
	ADD_TAG_DETAILS(getXpath("//span[text()=' Add tags']")),
	ENTER_TAG(getXpath("//div[@class='Select-placeholder']/following-sibling::div//input")),
	ADD_TAG(getXpath("//div[@aria-label='Press TAB to add']")),
	SEGMENTS_TAB(getXpath("//span[text()='Segments']")), 
	USERS_TAB(getXpath("//span[text()='Users']")),
	SEGMENTS_TAB_LIST(getXpath("//span[text()='Segments']/ancestor::li")),
	LIVE_SEGMENTS_TAB(getXpath("//span[text()='Live Segments']")),
	STATIC_LISTS_TAB(getXpath("//span[text()='Static Lists']")), 
	CREATE_BTN(getXpath("//a[contains(@href,'create')]")),
	CSV_SEGMENT_NAME(getXpath("//*[text()='SEGMENT NAME']/../following-sibling::div/div/input")),
	USER_CARD(getXpath("//span[text()='User ']")), 
	BEHAVIORAL_CARD(getXpath("//span[text()='Behavioral']")),
	TECHNOLOGY_CARD(getXpath("//span[text()='Technology']")), 
	ANDROID_CARD(getXpath("//span[text()='Android']")),
	IOS_CARD(getXpath("//span[text()='iOS']")), 
	WEB_CARD(getXpath("//span[text()='Web']")),
	ENTER_VALUE(getXpath("//*[text()='Enter a value']/following-sibling::div/input|//*[@class='form__element  col-4 react-select-wrapper-element']//input")),
	SEGMENT_DETAILS_BTN(getXpath("//*[text()='Segment Details']/button")),
	SAVE_BTN(getXpath("//button[text()='Save'] | //button[text()='save']")), 
	SPINNER(getClassName("loader")),
	POP_OVER_DELETE(getXpath("//*[text()='Delete']")), 
	POP_OVER_TAG(getXpath("//*[text()='Tag']")),
	DELETE_BTN(getXpath("//button[text()='Delete Segment']")),
	CONTAINER_TITLE(getXpath("//*[@class='modal__title']")),
	CLOSE_TOAST(getXpath("//*[@class='toast__close fl fl-close']")),
	SEGEMNT_PAGE_HEADER(getXpath("//*[@class='subhead']/descendant::h4")),
	USER_CARD_HEADER(getXpath("//*[text()='User ']/../../..")),
	BEHAVIORAL_CARD_HEADER(getXpath("//*[text()='Behavioral']/../../..")),
	TECHNOLOGY_CARD_HEADER(getXpath("//*[text()='Technology']/../../..")),
	ANDROID_CARD_HEADER(getXpath("//*[text()='Android']/../../..")),
	IOS_CARD_HEADER(getXpath("//*[text()='iOS']/../../..")), 
	WEB_CARD_HEADER(getXpath("//*[text()='Web']/../../..")),
	SEARCH_BAR(getXpath("//input[contains(@placeholder,'Search by')]")), 
	TABLE_HEADER(getXpath("//*[@class='table__header' or @class='card__header']")),
	USER_ROWS(getXpath("//div[@class='table__container ']/descendant::tbody/tr")),
	USER_ROWS_USER_ID(getXpath("//div[@class='table__container ']/descendant::tbody/tr/td/a")),
	FILTER_BTN(getXpath("//*[contains(@class,'fl-filter')]")),
	SELECTED_FILTERS(getXpath("//*[@class='label  checkbox__label--is-checked checkbox__label']")),
	TABLE_HEADERS(getXpath("//div[@class='table__container ']/descendant::th")),
	SELECT_OPERATOR(getXpath("//*[@class='row']/descendant::*[text()='- Select Operator -']")),
	ENTER_A_VALUE(getXpath("//*[@class='row']/descendant::*[text()='Enter a value' or contains(@class,'col-4 react-select-wrapper-element')]")),
	UPLOAD_FILE(getId("input-file-csv-static")),
	SHOWDETAILS(getXpath("//span[text()='Show details']")),
	OPEN_CALENDAR(getXpath("//div[@class='DateInput DateInput--open-down']")),
	OPEN_MONTH_LIST(getXpath("//div[@data-visible='true']//*[@id='CalendarMonth__caption']//span[1]")),
	CARD_CONTENT(getXpath("//*[contains(@class,'card__content')]"));;

	public static String TAG_DATE="//*[@title='%s1']/ancestor::tr//td//span[@class='tag-pill' and @title='%s2']";
	public static String VERIFY_TAG="//*[@title='%s']/ancestor::tr//td//span[@class='tag-pill']";
	public static String DELETE_TAG="//span[@title='%s']/preceding::span[@aria-hidden='true']";
	public static String OPEN_DD = "//*[text()='%s']/../following-sibling::div/descendant::div[contains(@class,'col') or contains(@class,'form')]";
	public static String OPEN_DD_SELECT = "//*[text()='%s1']/../following-sibling::div/descendant::div[contains(@class,'col') or contains(@class,'form')]/descendant::*[text()='%s2']";
	public static String OPEN_DD_COL2 = "//*[text()='%s']/../following-sibling::div/descendant::div[@class='col-6' or @class='col-4']";
	public static String SELECT_FROM_DD = "//*[@aria-label='%s' or @title='%s']";
	public static String SEGEMENT_DETAIL_COUNT = "//*[text()='%s ']/following-sibling::span/b";
	public static String SEGMENTS_LIST = "//tr[contains(@style,'height')]/descendant::a[@title='%s']";
	public static String NOTIFICATION_TOAST = "//*[text()='%s']";
	public static String OPEN_POP_OVER = "//*[@title='%s' or text()='%s']/ancestor::tr[contains(@style,'height')]/descendant::*[@class='pop-over ']";
	public static String PERFORM_ACTION = "//*[text()='%s']";
	public static String SELECT_EVENT="//*[@title='system']/%s1::*[@title='%s2']";
	public static String SELECT_APPLICATION_EVENT="//*[@title='application']/%s1::*[@title='%s2']";
	public static String OPEN_SEGMENT = "//tr[contains(@style,'height')]/descendant::*[@title='%s' or text()='%s']";
	public static String STATIC_SEGMENT_TOOL_TIP = "//tr[contains(@style,'height')]/descendant::*[@title='%s']/../../descendant::span[contains(@class,'status-label')]";
	public static String TOOLTIP_BUTTON = "//span[@data-tooltip='%s']";
	public static String USERS_SEGMENT_PAGE = "//*[text()='%s']/../following-sibling::h5";
	public static String SELECT_PLACE_HOLDER = "//*[text()='%s']/../../following-sibling::div/descendant::div[@class='Select-placeholder']";
	public static String CHECKBOX = "//label[contains(text(),'%s')]";
	public static String ADD_BUTTON = "//*[text()='%s']/../following-sibling::*/descendant::i[contains(@class,'add')]";
	public static String FILTER_BUTTON = "//*[text()='%s']/../following-sibling::*/descendant::i[contains(@class,'filter')]";
	public static String LOGICAL_OP = "//*[text()='%s1']/../following-sibling::div[contains(@class,'form__element') and contains(@class,'row')]/descendant::label[text()='%s2']";
	public static String SEGMENT_TOOLTIP="//div[.='%s']//i[@class='fl-info fl-dark']";
	public static String SEND_VALUE="//*[text()='%s1']/../following-sibling::div//input[@type='%s2']";
	public static String SELECT_MONTH="//div[contains (@class,'SingleDatePicker__picker overflow-hidden noselect')]//*[text()='%s']";
	public static String SELECT_DATE="//td/button[text()='%s1' and contains(@aria-label,'%s2')]";
	By locator;

	SegmentsPageElements(By locator) {
		this.locator = locator;
	}

	public By locate() {
		return this.locator;
	}

	static By getXpath(String element) {
		return By.xpath(element);
	}

	static By getId(String element) {
		return By.id(element);
	}

	static By getCSS(String element) {
		return By.cssSelector(element);
	}

	static By getClassName(String element) {
		return By.className(element);
	}

}
