package com.webengage.ui_automation.pojopackages.user;

public class Attributes {

	private String Age;
	private Boolean AutomationUser;
	private String Twitter_username;
	private Double Dollars_spent;
	private Integer Points_earned;

	public String getAge() {
		return Age;
	}

	public void setAge(String age) {
		this.Age = age;
	}

	public Boolean getAutomationUser() {
		return AutomationUser;
	}

	public void setAutomationUser(Boolean AutomationUser) {
		this.AutomationUser = AutomationUser;
	}

	public String getTwitterUsername() {
		return Twitter_username;
	}

	public void setTwitterUsername(String twitterUsername) {
		this.Twitter_username = twitterUsername;
	}

	public Double getDollarsSpent() {
		return Dollars_spent;
	}

	public void setDollarsSpent(Double dollarsSpent) {
		this.Dollars_spent = dollarsSpent;
	}

	public Integer getPointsEarned() {
		return Points_earned;
	}

	public void setPointsEarned(Integer pointsEarned) {
		this.Points_earned = pointsEarned;
	}

}