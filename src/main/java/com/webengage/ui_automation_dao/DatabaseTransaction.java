package com.webengage.ui_automation_dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.SegmentsPage;
import com.webengage.ui_automation.utils.DatabaseUtility;
import com.webengage.ui_automation.utils.Log;

public class DatabaseTransaction {

	Connection testExecutionConnection;
	Connection testDataConnection;
	private static DatabaseTransaction instance;
	String buildNumber = System.getProperty("set.BuildNumber");

	private DatabaseTransaction() {
		try {
			testExecutionConnection = DatabaseUtility.getConnection("we_testexecution");
			testDataConnection = DatabaseUtility.getConnection("we_testdata");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static DatabaseTransaction getInstance() {
		if (instance == null) {
			instance = new DatabaseTransaction();
		}
		return instance;
	}

	public void selectTable(String featureName, String scenarioName) {
		switch (featureName) {
		case "LiveSegments":
			insertIntoRespectiveTables(featureName, SegmentsPage.getSegmentId(), scenarioName);
			break;
		case "SMSCampaigns":
		case "EmailCampaigns":
		case "WhatsappCampaign":
			insertIntoRespectiveTables(featureName, CampaignsPage.getCampaignID(), scenarioName);
			break;
		default:
			Log.info("Not part of e2e suite for Data validation");
			break;
		}
	}

	private void insertIntoRespectiveTables(String feature, String uniqueId, String scenarioName) {
		RuntimeDataDAO dao = new RuntimeDataDAO();
		TransactionRecord tr = new TransactionRecord();
		tr.buildNumber = Integer.parseInt(buildNumber);
		tr.uniqueId = uniqueId;
		tr.scenarioName = scenarioName;
		if (feature.contains("Segments"))
			DatabaseUtility.executeUpdate(dao.insertRecordinRuntimeTable("segment", tr));
		else
			DatabaseUtility.executeUpdate(dao.insertRecordinRuntimeTable("campaign", tr));
	}

	public String fetchTemplateFromDb(String scenarioName) {
		String template = null;
		try {
			ResultSet resultSet = DatabaseUtility.executeQuery(testDataConnection,
					"select template from template_data where test_scenario='" + scenarioName.replace("'", "\\'")
					+ "'");
			if (resultSet.next())
				template = resultSet.getString("template");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return template;
	}
}
