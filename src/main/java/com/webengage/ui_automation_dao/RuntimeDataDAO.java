package com.webengage.ui_automation_dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RuntimeDataDAO {

	public PreparedStatement insertRecordinRuntimeTable(String module,TransactionRecord tr) {
		String query = "insert into "+module+"_runtime_data values (?,?,?)";
		try {
			PreparedStatement ps = DatabaseTransaction.getInstance().testExecutionConnection.prepareStatement(query);
			ps.setInt(1, tr.buildNumber);
			ps.setString(2, tr.uniqueId);
			ps.setString(3, tr.scenarioName);
			return ps;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}

class TransactionRecord {
	int buildNumber;
	String uniqueId;
	String scenarioName;
}