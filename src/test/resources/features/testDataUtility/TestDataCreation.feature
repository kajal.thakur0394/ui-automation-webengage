@TestDataCreation
Feature: Create and feed pre-requisite test data in user defined or newly created accounts via API
  I want to use this template for my feature file

  @InjectSegments
  Scenario: Segment Creation
    Given I select the function "Segments" for building the test data
    Given I select the function "StaticSegments" for building the test data

  @InjectCredentials
  Scenario: Inject Credentials
    Given I select the function "Integrations" for building the test data
    Given I select the function "SDK" for building the test data

  @InjectUsers
  Scenario: Create required Users
    Given I create all the users reuqired for test suite
    And I select the function "Users" for building the test data
    And hit "performEvent" API from "PerformEventsInject" sheet to trigger Event with reference id "orderPlaced"
    And hit "performEvent" API from "PerformEventsInject" sheet to trigger Event with reference id "triggerSMSEvent"
    And hit "performEvent" API from "PerformEventsInject" sheet to trigger Event with reference id "triggerEmailEvent"
    And hit "performEvent" API from "PerformEventsInject" sheet to trigger Event with reference id "triggerPushEvent"
    And hit "performEvent" API from "PerformEventsInject" sheet to trigger Event with reference id "addedToCart"

  @InjectTags
  Scenario: Attach tags to created segments and campaigns
    Given I select the function "Tags" for building the test data

  @InjectCampaigns
  Scenario: Create campaigns
    Given I select the channel "SMS" for campaign creation
    Given I select the channel "Email" for campaign creation
    Given I select the channel "Push" for campaign creation
    Given I select the channel "WebP" for campaign creation

  @InjectProperties
  Scenario: Inject Properties
    Given I select the function "InLineContentWebP" for building the test data
    Given I select the function "InLineContentAppP" for building the test data

  @InjectCatalogs
  Scenario: Inject Catalogs
    Given I select the function "Catalogs" for building the test data
