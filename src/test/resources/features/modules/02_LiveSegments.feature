@LiveSegments @Regression
Feature: Live Segments Feature

  @Smoke
  Scenario: Duplicate a newly created or existing segment
    Given I navigate to Live Segments Page
    And perform "Duplicate" action for "Duplicate this Segment" segment on the listing page
    Then verify the popup for Segment Duplication and duplicate it in same project itself
    Then verify that the segment "Copy of Duplicate this Segment" gets Listed on the page
    Then I create a Live segment with name as "Runtime Segment Duplication"
    And Pass following details in user card
      | Visitor Type | All Users  |
      | User IDs     | contains>4 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    And perform "Duplicate" action for "Runtime Segment Duplication" segment on the listing page
    Then verify the popup for Segment Duplication and duplicate it in same project itself
    Then verify that the segment "Copy of Runtime Segment Duplication" gets Listed on the page
    Then I delete the following mentioned segments
      | Copy of Duplicate this Segment      |
      | Copy of Runtime Segment Duplication |

  @Smoke
  Scenario: Edit an existing Segment by changing User Condition and save it.
    Given I navigate to Live Segments Page
    Given perform "Edit" action for "Runtime Segment Duplication" segment on the listing page
    Then I Edit the Segment
    And Pass following details in user card
      | Visitor Type | All Users                |
      | User IDs     | does not contain>AutoID1 |
    And Save the Segment
    Then check whether the segment has been "Updated" and gets listed on the page.

  @Smoke
  Scenario: Edit an existing Segment by changing Behavioural Condition and save it.
    Given I navigate to Live Segments Page
    Then I Open the Segment "Runtime Segment Duplication"
    Then I click on the "Edit" tool-tip
    Then I Edit the Segment
    And Pass following details in Behavioural card
      |  | Users who DID these events : | System>App Installed |
    And Save the Segment
    Then check whether the segment has been "Updated" and gets listed on the page.
    Then I delete segment with Name "Runtime Segment Duplication"

  Scenario: Check the count of users by refreshing Segment Details and on listing page
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "Count the Users"
    And Pass following details in user card
      | Visitor Type | All Users        |
      | User IDs     | equal to>AutoID1 |
    Then I click on refresh button and check "Total Users" count as 1
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Count the Users"
    Then I verify "Total Users" count as 1
    And I navigate to Live Segments Page
    Then I delete segment with Name "Count the Users"

  Scenario: Create a segment by using Technology Condition > Android
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "Technology Condition > Android"
    And Pass following details for "Android" in Technology card
      | Total Time Spent (in seconds) | greater than>100 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Technology Condition > Android"
    And I navigate to Live Segments Page
    Then I delete segment with Name "Technology Condition > Android"

  Scenario: Create a segment by using both User + Behavioural Condition
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "User + Behavioural Condition"
    And Pass following details in user card
      | Visitor Type | All Users             |
      | Reachability | Reachable on-WhatsApp |
    And Pass following details in Behavioural card
      |  | Users who DID NOT do these events : | System>App Installed |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User + Behavioural Condition"
    And I navigate to Live Segments Page
    Then I delete segment with Name "User + Behavioural Condition"

  Scenario: Create a segment by using User Condition > Reachability
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "User Condition > Reachability"
    And Pass following details in user card
      | Visitor Type | All Users             |
      | Reachability | Reachable on-WhatsApp |
      | Reachability | Reachable on-Web Push |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition > Reachability"
    And I navigate to Live Segments Page
    Then I delete segment with Name "User Condition > Reachability"

  Scenario: Create a segment by using User Condition > User IDs
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users     |
      | User IDs     | starts with>W |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition > User IDs"
    And I navigate to Live Segments Page
    Then I delete segment with Name "User Condition > User IDs"

  Scenario: Create a segment by using User Condition > Created On
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "User Condition > Created On"
    And Pass following details in user card
      | Created On | before-date |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Condition > Created On"
    And I navigate to Live Segments Page
    Then I delete segment with Name "User Condition > Created On"

  Scenario: Create a segment by using User Condition and adding more than 2 User Attribute condition
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "2 User Attribute condition"
    And Pass following details in user card
      | Geo Filtering | India        |
      | User IDs      | is not empty |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "2 User Attribute condition"
    And I navigate to Live Segments Page
    Then I delete segment with Name "2 User Attribute condition"

  Scenario: Create a segment by using User Condition > User Attribute > Custom Attribute
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "User Attribute > Custom Attribute"
    And Pass following details in user card
      | User Attribute>Select an Option | AutomationUser |
      | User Attribute>Select...        | equal to       |
      | User Attribute>Select...        | True           |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Attribute > Custom Attribute"
    And I navigate to Live Segments Page
    Then I delete segment with Name "User Attribute > Custom Attribute"

  Scenario: Create a segment by using User Condition > User Attribute > System Attribute
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "User Attribute > System Attribute"
    And Pass following details in user card
      | User Attribute>Select an Option | Country  |
      | User Attribute>Select...        | equal to |
      | User Attribute>Enter a value    | India    |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Attribute > System Attribute"
    And I navigate to Live Segments Page
    Then I delete segment with Name "User Attribute > System Attribute"

  Scenario: Create a segment by using Behavioural Condition and adding more than 1 condition of Users who did not do these Events.
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "more than 1 condition of Users who did not do these Events"
    And Pass following details in Behavioural card
      |    | Users who DID NOT do these events : | System>App Installed |
      | Or | Users who DID NOT do these events : | System>User Login    |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "more than 1 condition of Users who did not do these Events"
    And I navigate to Live Segments Page
    Then I delete segment with Name "more than 1 condition of Users who did not do these Events"

  Scenario: Create a segment by using Behavioural Condition and adding more than 1 condition of Users who did these Events.
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "more than 1 condition of Users who did these Events"
    And Pass following details in Behavioural card
      |     | Users who DID these events : | System>User Login    |
      | And | Users who DID these events : | System>App Installed |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "more than 1 condition of Users who did these Events"
    And I navigate to Live Segments Page
    Then I delete segment with Name "more than 1 condition of Users who did these Events"

  Scenario: Create a segment by using Behavioural Condition > Users who did not do these Events and add an Event Time Filter.
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "Users who did not do these Events and add an Event Time Filter."
    And Pass following details in Behavioural card
      |  | Users who DID NOT do these events : | System>User Login |
    And I apply the filter for "Users who DID NOT do these events :" as
      | Device | equal to | Huawei |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Users who did not do these Events and add an Event Time Filter."
    And I navigate to Live Segments Page
    Then I delete segment with Name "Users who did not do these Events and add an Event Time Filter."

  Scenario: Create a segment by using Behavioural Condition > Users who did these Events and add an Event Time Filter.
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "Users who did these Events and add an Event Time Filter"
    And Pass following details in Behavioural card
      |  | Users who DID these events : | System>App Installed |
    And I apply the filter for "Users who DID these events :" as
      | City | not equal to | Mumbai |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Users who did these Events and add an Event Time Filter"
    And I navigate to Live Segments Page
    Then I delete segment with Name "Users who did these Events and add an Event Time Filter"

  Scenario: Verify that user is able to create segments from the bulk imported users or events.
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "segments from the bulk imported users"
    And Pass following details in user card
      | Visitor Type | All Users        |
      | User IDs     | equal to>csvUser |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "segments from the bulk imported users"
    And I navigate to Live Segments Page
    Then I delete segment with Name "segments from the bulk imported users"

  Scenario: Verify existing tags is attached to Live Segments
    Given I navigate to Live Segments Page
    And I search for "LiveSegments Tag" segment
    Then I verify "livesegmentstag" tag is attached to the "LiveSegments Tag" segment

  Scenario: Verify new tag is created from Menu and gets attached to newly created Live Segment
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "Tags:User Condition > User IDs"
    And Pass following details for "Android" in Technology card
      | Total Time Spent (in seconds) | greater than>100 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    And I search for "Tags:User Condition > User IDs" segment
    And create new tag with name as "segmenttag" for segment "Tags:User Condition > User IDs"
    And I search for "Tags:User Condition > User IDs" segment
    Then I verify "segmenttag1" tag is attached to the "Tags:User Condition > User IDs" segment
    Then I navigate to Live Segments Page
    And I search for "Tags:User Condition > User IDs" segment
    Then delete the tag with name as "segmenttag" from segment "Tags:User Condition > User IDs"
    Then I delete segment with Name "Tags:User Condition > User IDs"

  Scenario: Verify new tag is created from Show Details and gets attached to newly created Live Segment
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "Tags:User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    And I search for "Tags:User Condition > User IDs" segment
    And create new tag from show details with name as "segmenttag" for segment "Tags:User Condition > User IDs"
    Then I navigate to Live Segments Page
    And I search for "Tags:User Condition > User IDs" segment
    Then I verify "segmenttag2" tag is attached to the "Tags:User Condition > User IDs" segment
    Then I navigate to Live Segments Page
    And I search for "Tags:User Condition > User IDs" segment
    Then delete the tag with name as "segmenttag" from segment "Tags:User Condition > User IDs"
    Then I delete segment with Name "Tags:User Condition > User IDs"

  @Smoke
  Scenario: To verify List of users download for a live segment
    Given I navigate to Live Segments Page
    And I Open the Segment "Duplicate this Segment"
    And I click on the "Download List of Users" tool-tip
    Then I download the list and verify it

  Scenario: To verify creation of segment with unicode characterset for Behavioural Condition > Users who did these Events
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "Unicode Behavioral:$ ! # % &༂ ༃ ༄ ༅ ခ ဂ अ आ इ ई ਆ ਇ"
    And Pass following details in Behavioural card
      |     | Users who DID these events : | System>User Login    |
      | And | Users who DID these events : | System>App Installed |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Unicode Behavioral:$ ! # % &༂ ༃ ༄ ༅ ခ ဂ अ आ इ ई ਆ ਇ"
    And I navigate to Live Segments Page
    Then I delete segment with Name "Unicode Behavioral:$ ! # % &༂ ༃ ༄ ༅ ခ ဂ अ आ इ ई ਆ ਇ"

  Scenario: To verify unicode characterset contents are updated properly on editing segments for Technological condition
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "Unicode Technological:ℙ ℚ ℛ ℜ ℝ ℞ ℟ℾ ℿ ⅀ ⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕ"
    And Pass following details for "Android" in Technology card
      | Total Time Spent (in seconds) | greater than>100 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Unicode Technological:ℙ ℚ ℛ ℜ ℝ ℞ ℟ℾ ℿ ⅀ ⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕ"
    Then I click on the "Edit" tool-tip
    Then I Edit the Segment
    And Pass following details for "iOS" in Technology card
      | Total Time Spent (in seconds) | less than>100 |
    And Save the Segment
    Then check whether the segment has been "Updated" and gets listed on the page.
    Then I Open the Segment "Unicode Technological:ℙ ℚ ℛ ℜ ℝ ℞ ℟ℾ ℿ ⅀ ⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕ"
    And I navigate to Live Segments Page
    Then I delete segment with Name "Unicode Technological:ℙ ℚ ℛ ℜ ℝ ℞ ℟ℾ ℿ ⅀ ⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕ"

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User who did this event AND User who did not do this event
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "Users who did these Events AND User who did not do this event"
    And Pass following details in Behavioural card
      |  | Users who DID these events :        | Application>E1 |
      |  | Users who DID NOT do these events : | Application>E2 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Users who did these Events AND User who did not do this event"
    And I navigate to Live Segments Page

  #@End-to-End @Distributed
  #Scenario: To verify the segment creation with visitor type and created on is On selected date
  #Given I navigate to Live Segments Page
  #Then I create a Live segment with name as "User with visitor type and created on selected date"
  #And Pass following details in user card
  #| Created On  | on     |
  #| Set Date as | 19 Jan |
  #And Save the Segment
  #Then check whether the segment has been "Created" and gets listed on the page.
  #Then I Open the Segment "User with visitor type and created on selected date"
  #And I navigate to Live Segments Page
  @End-to-End @Distributed
  Scenario: To verify the segment creation with User who did this event with Custom Event applying custom attribute filter
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "custom event creation for event E1 and price filter"
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | equal to>LS1 |
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>E1 |
    And I apply the filter for "Users who DID these events :" as
      | Price | equal to | 1000 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "custom event creation for event E1 and price filter"
    And I navigate to Live Segments Page

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User who did this event with System Event applying custom attribute filter
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "system event creation for event email open and email id filter"
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | equal to>LS1 |
    And Pass following details in Behavioural card
      |  | Users who DID these events : | System>Email Open |
    And I apply the filter for "Users who DID these events :" as
      | email_id | one of | aditya.dhanve@webengage.com |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "system event creation for event email open and email id filter"
    And I navigate to Live Segments Page

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User System Attribute First Name does not start with selected dropdown value
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "User Attribute > first name"
    And Pass following details in user card
      | Visitor Type                    | All Users           |
      | User Attribute>Select an Option | First Name          |
      | User Attribute>Select...        | does not start with |
      | User Attribute>Enter a value    | Test                |
      | User IDs                        | equal to>LS1        |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Attribute > first name"
    And I navigate to Live Segments Page

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User who did this event with Custom Event applying system attribute filter
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "custom event creation for event E1 and browser name filter"
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | equal to>LS1 |
    And Pass following details in Behavioural card
      |  | Users who DID these events : | Application>E1 |
    And I apply the filter for "Users who DID these events :" as
      | Browser Name | contains | Chrome |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "custom event creation for event E1 and browser name filter"
    And I navigate to Live Segments Page

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User Id equal to entered value
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "All user with User Ids equals to LS1"
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | equal to>LS1 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "All user with User Ids equals to LS1"
    And I navigate to Live Segments Page

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User Custom Attribute Age equal to selected value 29
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "User Attribute age equals to 29"
    And Pass following details in user card
      | Visitor Type                    | All Users |
      | User Attribute>Select an Option | Age       |
      | User Attribute>Select...        | equal to  |
      | User Attribute>Enter a value    |        29 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "User Attribute age equals to 29"
    And I navigate to Live Segments Page

  @End-to-End @Distributed
  Scenario: To verify the segment creation with User Id starts with entered value
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "All user with User starts with LS"
    And Pass following details in user card
      | Visitor Type | All Users      |
      | User IDs     | starts with>LS |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "All user with User starts with LS"
    And I navigate to Live Segments Page

  @End-to-End @Distributed @skip @issue:WP-17536
  Scenario: To verify the segment creation with User who did this event OR User who did not do these event.
    Given I navigate to Live Segments Page
    Then I create a Live segment with name as "Application event creation for Users who DID these events E1 or Users who did not E2 "
    And Pass following details in user card
      | Visitor Type | All Users    |
      | User IDs     | contains>LS1 |
    And Pass following details in Behavioural card
      |  | Users who DID these events :        | Application>E1 |
      |  | Users who DID NOT do these events : | Application>E2 |
    And Save the Segment
    Then check whether the segment has been "Created" and gets listed on the page.
    Then I Open the Segment "Application event creation for Users who DID these events E1 or Users who did not E2 "
    And I navigate to Live Segments Page
