@InLineContentAppP @Regression
Feature: In-line Content App Personalization Feature

  @Smoke
  Scenario: To verify user is able to send InlineApp campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "Test campaign CRUD" Campaign Name
    Then select "Dummy: Android and iOS (Android: Test, iOS: Test)" as property
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select "Text View" layout
    Then fill the page via "In-lineAppPersonalisation" template from "PersonalizationTemplates" folder using "TextAndAction" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "Test campaign CRUD" campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "Test campaign CRUD"

  @Smoke
  Scenario: To verify Schedule Campaign with Start date: Now and End date: Never is configured
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "Test Schedule: Start Now and End Later" Campaign Name
    Then select "Dummy: Android and iOS (Android: Test, iOS: Test)" as property
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | START DATE | now  |  |  |
      | END DATE   | till |  |  |
    Then I select "Text View" layout
    Then fill the page via "In-lineAppPersonalisation" template from "PersonalizationTemplates" folder using "TextAndAction" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "Test Schedule: Start Now and End Later" campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    And I verify following details for "Test Schedule: Start Now and End Later" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "Test Schedule: Start Now and End Later"

  Scenario: To verify overview page is loaded when campaign status is paused for InLineContent App
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "InlineContent-App-Overview Page Validation for Paused status" Campaign Name
    Then select "Dummy: Android and iOS (Android: Test, iOS: Test)" as property
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select "Text View" layout
    Then fill the page via "In-lineAppPersonalisation" template from "PersonalizationTemplates" folder using "TextAndAction" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then I pause the campaign with "inlineContent-appP-deactivate" api
    Then check overview page is loaded for "InlineContent-App-Overview Page Validation for Paused status" campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "InlineContent-App-Overview Page Validation for Paused status"

  Scenario: To verify overview page is loaded when campaign status is in draft state for InLineContent App
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "InlineContent-App-Overview Page Validation for draft status" Campaign Name
    Then select "Dummy: Android and iOS (Android: Test, iOS: Test)" as property
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select "Text View" layout
    Then fill the page via "In-lineAppPersonalisation" template from "PersonalizationTemplates" folder using "TextAndAction" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    Given I navigate to "In-line Content" page via "App Personalization"
    Then check overview page is loaded for "InlineContent-App-Overview Page Validation for draft status" campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "InlineContent-App-Overview Page Validation for draft status"

  @MobileSDKSuite
  Scenario: To verify InlineApp campaign having Banner layout with Image URL & On-click redirection to screen is delivered successfully
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "App-In-Line campaign: Banner" Campaign Name
    Then select "Android only (Android: S1P1)" as property
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Banner View" layout
    Then fill the page via "In-lineAppPBanner-URL-screen" template from "PersonalizationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then wait for "fetchInlineContentAppP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: To verify InlineApp campaign having Banner layout with Image Upload & On-click redirection to different app is delivered successfully
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "App-In-Line campaign: Banner" Campaign Name
    Then select "Android only (Android: S1P1)" as property
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Banner View" layout
    Then fill the page via "In-lineAppPBanner-Upload-App" template from "PersonalizationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then wait for "fetchInlineContentAppP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: To verify InlineApp campaign having Banner layout with Keyword Image is delivered successfully
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "App-In-Line campaign: Banner" Campaign Name
    Then select "Android only (Android: S1P1)" as property
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Banner View" layout
    Then fill the page via "In-lineAppPBanner-Keyword" template from "PersonalizationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then wait for "fetchInlineContentAppP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: To verify in-line-app campaign having Banner layout unicode characterset is delivered successfully
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "Unicode:App-In-Line campaign: Banner ℮ ℯ ℰ ℱ Ⅎ ℳ ℴ ℵ ℶ ई ਆ" Campaign Name
    Then select "Android only (Android: S1P1)" as property
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Banner View" layout
    Then fill the page via "UnicodeIn-lineAppPBanner" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    And I verify "Unicode:App-In-Line campaign: Banner ℮ ℯ ℰ ℱ Ⅎ ℳ ℴ ℵ ℶ ई ਆ" Campaign name with unicode characters on overview page
    Then wait for "fetchInlineContentAppP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: To verify InlineApp campaign having Text layout with Image URL & On-click redirection to screen is delivered successfully
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "App-In-Line campaign: Text" Campaign Name
    Then select "Android only (Android: S1P1)" as property
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Text View" layout
    Then fill the page via "In-lineAppPText-URL-screen" template from "PersonalizationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then wait for "fetchInlineContentAppP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: To verify InlineApp campaign having Text layout with Image URL & On-click redirection to screen is delivered successfully
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "App-In-Line campaign: Text" Campaign Name
    Then select "Android only (Android: S1P1)" as property
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Text View" layout
    Then fill the page via "In-lineAppPText-Keyword" template from "PersonalizationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then wait for "fetchInlineContentAppP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: To verify InlineApp campaign having Text layout with Image URL & On-click redirection to screen is delivered successfully
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "App-In-Line campaign: Text" Campaign Name
    Then select "Android only (Android: S1P1)" as property
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Text View" layout
    Then fill the page via "In-lineAppPText-Upload-App" template from "PersonalizationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then wait for "fetchInlineContentAppP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And export the template for Validation on Mobile Device

  @MobileSDKSuite
  Scenario: To verify InlineApp campaign having Custom layout is delivered successfully
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "App-In-Line campaign: Custom" Campaign Name
    Then select "Custom View (Android: flPersonalize4)" as property
    Then select Segment having name as "AndroidSDKUser"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Custom View" layout
    Then fill the page via "In-lineAppPCustom" template from "PersonalizationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then wait for "fetchInlineContentAppP" api to return response as "Running" for path as "response.data.experiment.sentStatus.status"
    And export the template for Validation on Mobile Device

  Scenario: To verify App-Inline campaign creation using inclusion/exclusion of segment feature
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Campaign Type and enters "LiveSegment with Inclusion/Exclusion of segment App-Inline Content Campaign" Campaign Name
    Then select "Dummy: Android (Android: Test)" as property
    Then I fill the details in Audience tab as below
      | Audience Type | multiple segments |
    Then I select "Users in ANY of these segments" as "Send To" Inclusion Segment
    And I add Segment Name as "App-Inline: User Condition > User ID ends with P"
    And Pass following details in user card And I Save the segment
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    Then I select "Users in ANY of these segments" as "EXCLUDE" Exclusion Segment
    And I add Segment Name as "App-Inline: User Condition > Reachability on Push"
    And Pass following details in user card And I Save the segment
      | Visitor Type | All Users         |
      | Reachability | Reachable on-Push |
    And I save the Audience details
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Text View" layout
    Then fill the page via "In-lineAppPText-Upload-App" template from "PersonalizationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    And verify via "fetchInlineContentAppP" API whether following segments have been attached to campaign just created
      | App-Inline: User Condition > User ID ends with P  |
      | App-Inline: User Condition > Reachability on Push |
    Then I navigate to Live Segments Page
    And I verify following Segments are present
      | App-Inline: User Condition > User ID ends with P  |
      | App-Inline: User Condition > Reachability on Push |
    Given I navigate to "In-line Content" page via "App Personalization"
    Then check overview page is loaded for "LiveSegment with Inclusion/Exclusion of segment App-Inline Content Campaign" campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "LiveSegment with Inclusion/Exclusion of segment App-Inline Content Campaign"
    And I navigate to Live Segments Page
    Then I delete the following mentioned segments
      | App-Inline: User Condition > User ID ends with P  |
      | App-Inline: User Condition > Reachability on Push |

  Scenario Outline: To verify previously created App-Inline Content campaigns are available in App Inline campaign > List of campaigns page
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I add "<CampaignName>" to searchbox
    And I verify that "<CampaignName>" and "<Layout>" exist

    Examples: 
      | CampaignName                             | Layout      |
      | Test Text View App Inline Campaign exist | Text View   |
      | Test Banner App Inline Campaign exist    | Banner View |
      | Test Custom App Inline Campaign exist    | Custom View |

  Scenario: To verify creation of Segments using segment editor in App-Inline
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Campaign Type and enters "Segment creation via Segment Editor-AppInline Campaign" Campaign Name
    Then select "Dummy: Android (Android: Test)" as property
    And I select New Segment Creation icon
    And I add Segment Name as "App-Inline: User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And I Save and verify the created Segment
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Text View" layout
    Then fill the page via "In-lineAppPText-Upload-App" template from "PersonalizationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    And verify via "fetchInlineContentAppP" API whether following segments have been attached to campaign just created
      | App-Inline: User Condition > User IDs |
    Then I navigate to Live Segments Page
    And I verify "App-Inline: User Condition > User IDs" segment is present
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "Segment creation via Segment Editor-AppInline Campaign"
    And I navigate to Live Segments Page
    Then I delete the following mentioned segments
      | App-Inline: User Condition > User IDs |

  Scenario: To verify on triggering conversion event, conversion is recorded for App-Inline campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Campaign Type and enters "Conversion Tracking App-Inline Campaign" Campaign Name
    Then select "Dummy: Android (Android: Test)" as property
    And I select New Segment Creation icon
    And I add Segment Name as "App-Inline: User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And I Save and verify the created Segment
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | SET TIMEOUT | 5000 |  |  |
    Then I select "Text View" layout
    Then fill the page via "In-lineAppPText-Upload-App" template from "PersonalizationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      | Conversion Tracking | On              |
      | CONVERSION EVENT    | TriggerSMSEvent |
      | CONVERSION DEADLINE | 7-Days          |
    And I launch the Campaign
    Then check overview page is loaded for "Conversion Tracking App-Inline Campaign" campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "triggerSMSEvent"
    Then I delete the Campaign with Name "Conversion Tracking App-Inline Campaign"
    And I navigate to Live Segments Page
    Then I delete the following mentioned segments
      | App-Inline: User Condition > User IDs |

  Scenario: To Verify existing tags is attached to InlineApp campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    And I search for "InlineApp campaign: Tag" campaign
    Then I verify "campaigntag" tag is attached to the "InlineApp campaign: Tag" campaign

  Scenario: Verify new tag is created from InlineApp Channel and gets attached to newly created InlineApp Campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "Tag: InlineApp Campaign" Campaign Name
    And create new tag from with name as "inlineapp-tag" for campaign
    Then select "Dummy: Android and iOS (Android: Test, iOS: Test)" as property
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      |  |  |  |  |
    Then I select "Text View" layout
    Then fill the page via "In-lineAppPersonalisation" template from "PersonalizationTemplates" folder using "TextAndAction" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "Tag: InlineApp Campaign" campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I verify "inlineapp-tag" tag is attached to the "Tag: InlineApp Campaign" campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    And I search for "Tag: InlineApp Campaign" campaign
    Then delete the tag with name as "inlineapp-tag" from campaign "Tag: InlineApp Campaign"
    Then I delete the Campaign with Name "Tag: InlineApp Campaign"

  Scenario: To verify Banner InlineApp campaign when Start Date: Now and End Date: Till is selected
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "InlineApp campaign with Start Date:Now and End Date:Till" Campaign Name
    Then select "Dummy: Android (Android: Test)" as property
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | START DATE | now  |  |  |
      | END DATE   | till |  |  |
    Then I select "Banner View" layout
    Then fill the page via "UnicodeIn-lineAppPBanner" template from "MultiLingualTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "InlineApp campaign with Start Date:Now and End Date:Till" campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I add "InlineApp campaign with Start Date:Now and End Date:Till" to searchbox
    And I verify following details for "InlineApp campaign with Start Date:Now and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "InlineApp campaign with Start Date:Now and End Date:Till"

  Scenario: To verify Banner InlineApp campaign when Start Date: Later and End Date: Till is selected
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "InlineApp campaign with Start Date:Later and End Date:Till" Campaign Name
    Then select "Dummy: iOS (iOS: Test)" as property
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | START DATE | later |  |  |
      | END DATE   | till  |  |  |
    Then I select "Banner View" layout
    Then fill the page via "In-lineAppPBanner-iOS" template from "PersonalizationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "InlineApp campaign with Start Date:Later and End Date:Till" campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I add "InlineApp campaign with Start Date:Later and End Date:Till" to searchbox
    And I verify following details for "InlineApp campaign with Start Date:Later and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "InlineApp campaign with Start Date:Later and End Date:Till"

  Scenario: To verify Text in-lineApp campaign when Start Date: Later and End Date: Never is selected
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "InlineApp campaign with Start Date:Later and End Date:Never" Campaign Name
    Then select "Dummy: Android and iOS (Android: Test, iOS: Test)" as property
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | START DATE | later |  |  |
      | END DATE   | never |  |  |
    Then I select "Text View" layout
    Then fill the page via "In-lineAppPersonalisation" template from "PersonalizationTemplates" folder using "TextAndAction" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "InlineApp campaign with Start Date:Later and End Date:Never" campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I add "InlineApp campaign with Start Date:Later and End Date:Never" to searchbox
    And I verify following details for "InlineApp campaign with Start Date:Later and End Date:Never" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "InlineApp campaign with Start Date:Later and End Date:Never"

  Scenario: To verify Custom InlineApp campaign when Start Date: Later and End Date: Till is selected
    Given I navigate to "In-line Content" page via "App Personalization"
    And I select "" Push Campaign Type and enters "InlineApp campaign with Start Date:Later and End Date:Till" Campaign Name
    Then select "Dummy: Android and iOS (Android: Test, iOS: Test)" as property
    Then select Segment having name as "User Condition > User IDs> ends with J"
    Then I fill the details in Audience tab as below
      |  |  |
    Then I fill the details in WHEN tab as below
      | START DATE | later |  |  |
      | END DATE   | till  |  |  |
    Then I select "Custom View" layout
    Then fill the page via "in-lineAppPCustom" template from "PersonalizationTemplates" folder using "Message" section
    Then I fill the details in Conversion Tracking tab as below
      |  |  |
    And I launch the Campaign
    Then check overview page is loaded for "InlineApp campaign with Start Date:Later and End Date:Till" campaign
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I add "InlineApp campaign with Start Date:Later and End Date:Till" to searchbox
    And I verify following details for "InlineApp campaign with Start Date:Later and End Date:Till" campaign
      | Start date: |
      | End date:   |
    Given I navigate to "In-line Content" page via "App Personalization"
    Then I delete the Campaign with Name "InlineApp campaign with Start Date:Later and End Date:Till"
