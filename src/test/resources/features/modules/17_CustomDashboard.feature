@CustomDashboard @Regression
Feature: Custom Dashboard Feature

  Scenario: To verify unicode characterset contents are showing properly when new dashboard is created
    Given I navigate to "Dashboards" Page
    And create new Dashboard with name as "Unicode:$ ! # % & ( ) * + , - . ' / : ; < = > ? @ [ ] ^ _ ` { } ~"
    And select dashboard visibility as "All team members can edit" and create a dashboard
    Then verify "Unicode:$ ! # % & ( ) * + , - . ' / : ; < = > ? @ [ ] ^ _ ` { } ~" name which has unicode characters on Overview page
    Given I navigate to "Dashboards" Page
    Then delete the module with Name "Unicode:$ ! # % & ( ) * + , - . ' / : ; < = > ? @ [ ] ^ _ ` { } ~"
