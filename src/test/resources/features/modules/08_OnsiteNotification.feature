@OnsiteNotification @Regression
Feature: On-site Notification Feature

  Scenario Outline: To verify previously created onsite-notification campaigns are available in onsite-notificationt > List of campaigns page
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    Then I add "<CampaignName>" to the searchbox
    And I verify that "<CampaignName>" and the "<LayoutType>" exist

    Examples: 
      | CampaignName                                    | LayoutType |
      | Test create your own: Banner layout exist       | Box        |
      | Test choose from gallery: CartAbandonment exist | Callout    |

  @WebSDKSuite
  Scenario: Verify on webpage that onsite-notification is getting delivered when ‘Create your own’ Notification is selected
    Given I navigate to "On-site Notifications" page via "Web Personalization"
    Then create a " Create Your Own " Notification and select "Box" template
    Then use the personalization template "onsite" for "On-site Notifications" web-p
    And open the dynamic webpage and login
    Then verify notification on a web page using template "onsite"
    Then delete the notification using template "onsite"
