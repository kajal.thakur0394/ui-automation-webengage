@StaticSegments @Regression
Feature: Static Segments Feature

  @Smoke
  Scenario: To verify creation of Static Segments using segment editor
    Given I navigate to Static Segments Page
    Then I create a Static Segment using "Segment Editor" with name "Static Segments using segment editor"
    And Pass following details in user card
      | Visitor Type | All Users       |
      | User IDs     | equal to>AutoID |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I verify that the Static segment is displayed on Listing Page
    Then I delete static segment with Name "Static Segments using segment editor"

  @Smoke
  Scenario: To verify creation of Static Segments using csv editor
    Given I navigate to Static Segments Page
    Then I create a Static Segment using "CSV Editor" with name "Static Segments using CSV editor"
    And I enter segment name "Static Segments using CSV editor" and select file from test data to upload
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I verify that the Static segment is displayed on Listing Page
    Then I delete static segment with Name "Static Segments using CSV editor"

  Scenario: Verify existing tags is attached to Static Segments
    Given I navigate to Static Segments Page
    And I search for "StaticSegments Tag" segment
    Then I verify "staticsegmentstag" tag is attached to the "StaticSegments Tag" segment

  Scenario: Verify new tag is created from Menu and gets attached to newly created Static Segment
    Given I navigate to Static Segments Page
    Then I create a Static Segment using "Segment Editor" with name "Tags:User Condition > User IDs"
    And Pass following details in user card
      | Visitor Type | All Users   |
      | User IDs     | ends with>P |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    And I search for "Tags:User Condition > User IDs" static segment
    And create new tag with name as "segmenttag3" for segment "Tags:User Condition > User IDs"
    Then I verify "segmenttag" tag is attached to the static segment
    Then I navigate to Static Segments Page
    And I search for "Tags:User Condition > User IDs" static segment
    Then delete the tag with name as "segmenttag" from segment "Tags:User Condition > User IDs"
    Then I delete static segment with Name "Tags:User Condition > User IDs"

  Scenario: To verify creation of Static Segment containing unicode characterset using Segment editor for UserID condition
    Given I navigate to Static Segments Page
    Then I create a Static Segment using "Segment Editor" with name "Unicode Segment Editor:# % & ( ) * ༂ ༃ ༄ ༅ ခ  ℚ ℛ ℜ ℝ ℞ ℟ℾ ℿ"
    And Pass following details in user card
      | Visitor Type | All Users       |
      | User IDs     | equal to>AutoID |
    And Save the Segment
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I verify that the Static segment is displayed on Listing Page
    Then I delete static segment with Name "Unicode Segment Editor:# % & ( ) * ༂ ༃ ༄ ༅ ခ  ℚ ℛ ℜ ℝ ℞ ℟ℾ ℿ"

  Scenario: To verify creation of Static Segments with unicode characterset using CSV upload
    Given I navigate to Static Segments Page
    Then I create a Static Segment using "CSV Editor" with name "Unicode CSV Editor:⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕअ आ इ ई ਆ ਇ ਈ"
    And I enter segment name "Unicode CSV Editor:⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕअ आ इ ई ਆ ਇ ਈ" and select file from test data to upload
    Then check whether the static segment has been "Created" and gets listed on the page.
    Then I verify that the Static segment is displayed on Listing Page
    Then I delete static segment with Name "Unicode CSV Editor:⅁ ⅂ ⅃ ⅄ ⅅ ⅆ ⅇ ⅈ ⅉ ℐ ℑ ℒ ℓ ℔ ℕअ आ इ ई ਆ ਇ ਈ"
