@Analytics @Regression
Feature: Analytics Feature

  @Smoke
  Scenario: To verify that Events page is loading and displaying the stats on all the required sections
    Given I navigate to "Events" page via "Analytics"
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "triggerSMSEvent"
    And hit "performEvent" API from "PerformEvents" sheet to trigger Event with reference id "triggerEmailEvent"
    Then set event section filter as
      | SHOW     | Occurrences       |
      | OF       | All Custom Events |
      | OVER     | -                 |
      | SPLIT BY | -                 |
    Given open table view for the section with header name as "SHOW"
    And verify following things get reflected in populated table
      | TriggerSMSEvent   | >2 |
      | TriggerEmailEvent | >2 |

  @Smoke
  Scenario: To verify that Paths shows the result for the selected values in Paths fields once clicked on "Show Path" Starting with
    Given I navigate to "Paths" page via "Analytics"
    Then fill the Path filter using below values
      | SHOW PATHS               | Starting with>Starting with | ->TriggerEmailEvent |
      | LOOKAHEAD WINDOW         |                          99 | hours>minutes       |
      | COLLAPSE REPEATED EVENTS | false                       |                     |
    Then validate whether event "TriggerEmailEvent" gets reflected in Step "1"

  @Smoke
  Scenario: To verify that in Events page it allows downloading the report successfully
    Given I navigate to "Events" page via "Analytics"
    Then set event section filter as
      | SHOW     | Occurrences     |
      | OF       | TriggerSMSEvent |
      | OVER     | Hours of Day    |
      | SPLIT BY | Country         |
    Then I download the report
      |  | Show Event |

  @Smoke
  Scenario: To verify that Paths shows the result for the selected values in Paths fields once clicked on "Show Path" Ending with
    Given I navigate to "Paths" page via "Analytics"
    Then fill the Path filter using below values
      | SHOW PATHS               | Starting with>Ending with | ->TriggerSMSEvent |
      | LOOKAHEAD WINDOW         |                        72 | hours>hours       |
      | COLLAPSE REPEATED EVENTS | false                     |                   |
    Then validate whether event "TriggerSMSEvent" gets reflected in Step "1"

  Scenario: To verify unicode characterset contents are showing properly when new funnel is created
    Given I navigate to "Funnels" page via "Analytics"
    And create new Funnel with name as "UTF: { } ~ ¢ £ ¤ ڝ ڞ ڟ ڠ ڡ अ आ इ ई ਆ ਇ ਈ ஆ இ"
    Then fill the Steps detail using below values
      | Step 1 | TriggerEmailEvent |
      | Step 2 | TriggerSMSEvent   |
    Given I navigate to "Funnels" page via "Analytics"
    Then verify "UTF: { } ~ ¢ £ ¤ ڝ ڞ ڟ ڠ ڡ अ आ इ ई ਆ ਇ ਈ ஆ இ" name which has unicode characters on Overview page
    Given I navigate to "Funnels" page via "Analytics"
    Then delete the funnel with name as "UTF: { } ~ ¢ £ ¤ ڝ ڞ ڟ ڠ ڡ अ आ इ ई ਆ ਇ ਈ ஆ இ"
