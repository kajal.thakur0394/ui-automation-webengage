@Recommendations @Regression
Feature: Recommendations feature

  Scenario: To verify unicode characterset contents are showing properly when new Recommendations is created
    Given I navigate to "Recommendations" Page
    And create recommendations using details below
      | Strategy Name        | Unicode:℞ ℟ ℠ ℡ ™ ℣ ℤ ℥ |
      | Catalog              | Test Catalog:exist      |
      | Type                 | Static                  |
      | Recommendation Event | Added to Cart           |
    Then verify "Unicode:℞ ℟ ℠ ℡ ™ ℣ ℤ ℥" recommendations is created with "Test Catalog:exist" catalog name
    Given I navigate to "Recommendations" Page
    Then delete the module with Name "Unicode:℞ ℟ ℠ ℡ ™ ℣ ℤ ℥"
