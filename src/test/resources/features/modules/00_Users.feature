@Users @Regression
Feature: Users feature

  @Smoke
  Scenario: To verify that in User Profile the Basic Info page is loading and displaying all the required detail
    Given I navigate to "List of Users" tab on Users Page
    Then I open user profile of a user having userId as "KajalP"
    And verify the following labels from "Basic Info" section
      | name  | Kajal P                    |
      | Email | kajal.thakur@webengage.com |
      | Phone | +919930630394              |

  @Smoke
  Scenario: To verify that in User Profile the Attribute page is loading and displaying all the required details
    Given I navigate to "List of Users" tab on Users Page
    Then I open user profile of a user having userId as "AutoID1"
    And verify the following labels from "Attributes" section
      | Gender  | Male      |
      | Company | WebEngage |

  @Smoke
  Scenario: To verify that in User Profile the Device page is loading and displaying all the required details
    Given I navigate to "List of Users" tab on Users Page
    Then I open user profile of a user having userId as "KajalP"
    And verify the following labels from "Devices" section for type "iOS" and device as "Device 1"
      | Device Manufacturer | Apple |

  @Smoke
  Scenario: To verify that in User Profile the Channel page is loading and displaying all the required details
    Given I navigate to "List of Users" tab on Users Page
    Then I open user profile of a user having userId as "KajalP"
    And verify the following labels from "Channels" section
      | Email    | green |
      | SMS      | green |
      | ON SITE  | red   |
      | WEB PUSH | red   |

  @Smoke
  Scenario: To verify that in User Profile the Events page is loading and displaying all the required details
    Given I navigate to "List of Users" tab on Users Page
    Then I open user profile of a user having userId as "KajalP"
    Then I navigate to events section and select period as "Last 30 days"

  @Smoke
  Scenario: Verify that user is able to search or apply filter on list of user page.
    Given I navigate to "List of Users" tab on Users Page
    Then I add "AutoID1" to search
    And verify the count of row as 1 and User Id as "AutoID1"
    Then I apply filter and select following columns
      | Name | Email | Phone | AutomationUser | Created On | Country |
    And check for presence of columns on listing page
      | User ID | Name | Email | Phone | AutomationUser | Created On | Country |

  @Smoke
  Scenario: To verify that Users page is loading and displaying the stats on all the required sections
    Given I navigate to "Overview" tab on Users Page
    Then check the count of following parameters in indiv cards
      | TOTAL USERS                | >2 |
      | KNOWN USERS                | >2 |
      | MONTHLY ACTIVE USERS (MAU) | >0 |

  @Smoke
  Scenario: To verify that Analyze page is loading and displaying the stats on all the required sections
    Given I navigate to "Overview" tab on Users Page
    Given open table view for the section with header name as "Activity (MAU, WAU, DAU)"
    And verify following things get reflected in populated table
      | formerDate | >0 |
    Given I navigate to "Analyze" tab on Users Page
    Given open table view for the section with header name as "Analyze"
    And verify following things get reflected in populated table
      | Total | >2 |

  @Smoke
  Scenario: To verify that in the Users page Activity Section and Users Analyze page it allows downloading the report successfully.
    Given I navigate to "Overview" tab on Users Page
    Then I download the report
      | Overview | Activity (MAU, WAU, DAU) |
      | Overview | Channel Reachability     |
      | Analyze  | Analyze                  |

  @Smoke
  Scenario: To verify that in the List of Users page it allows downloading the report successfully
    Given I navigate to "List of Users" tab on Users Page
    Then I download the report for list of users
