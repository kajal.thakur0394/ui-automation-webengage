@OnsiteSurveys @Regression
Feature: On-site Surveys Feature

  @Smoke
  Scenario: Verify on webpage that onsite-surveys is getting delivered
    Given I navigate to "On-site Surveys" page via "Web Personalization"
    Then create a " Create Your Own " Notification and select "On-site Classic" template
    Then use the personalization template "onsite-surveys" for "On-site Surveys" web-p
    Then delete the notification using template "onsite-surveys"
