@CampaignStats
Feature: Campaign Stats

  @Stats
  Scenario Outline: To verify table data across several channels
    Given I source the "DS" stats for "<Channel>"

    Examples: 
      | Channel             |
      | SMS                 |
      | Email               |
      | Whatsapp            |
      | Push                |
      | Web-Push            |
      | In-Line Content     |
      | In-App Notification |
