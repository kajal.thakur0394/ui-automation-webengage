package com.webengage.ui_automation.runners;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import com.webengage.ui_automation.driver.DriverFactory;
import com.webengage.ui_automation.initialize.RemoteConnection;
import com.webengage.ui_automation.utils.Log;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity; 

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = {"<featureFile>"}
,plugin = {"junit:target/cucumber-reports/<className>Cucumber.xml","json:target/cucumber-reports/<className>Cucumber.json"}
,glue={"com.webengage.ui_automation.stepDefinitions","com.webengage.ui_automation.hooks"}
,monochrome=true
,tags= "<tag>"
)

public class <className> {
public static LinkedList<LinkedHashMap<String, String>> scenarioDetails = new LinkedList<>();

	@BeforeClass
	public static void initialize() throws IOException, InterruptedException, ParseException {
		Log.info("...Inside <className>...");
		BeforeTest bt = new BeforeTest();
		bt.beforeTest();
	}

	@AfterClass
	public static void tearDown() throws IOException {
		DriverFactory.getInstance().closeBrowser();
		System.clearProperty("featureFileName");
		RemoteConnection.exportCampaignPayloads(System.getProperty("set.BuildNumber") == null);
	}}