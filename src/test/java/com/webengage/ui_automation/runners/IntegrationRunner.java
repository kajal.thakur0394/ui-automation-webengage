package com.webengage.ui_automation.runners;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import com.webengage.ui_automation.driver.DriverFactory;
import com.webengage.ui_automation.initialize.RemoteConnection;
import com.webengage.ui_automation.utils.Log;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity; 

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = {"src/test/resources/features/modules/04_Integration.feature"}
,plugin = {"junit:target/cucumber-reports/IntegrationRunnerCucumber.xml","json:target/cucumber-reports/IntegrationRunnerCucumber.json"}
,glue={"com.webengage.ui_automation.stepDefinitions","com.webengage.ui_automation.hooks"}
,monochrome=true
,tags= "((not @disabled and not @End-to-End and not @WebSDKSuite and not @MobileSDKSuite)) and (@Regression)"
)

public class IntegrationRunner {
public static LinkedList<LinkedHashMap<String, String>> scenarioDetails = new LinkedList<>();

	@BeforeClass
	public static void initialize() throws IOException, InterruptedException, ParseException {
		Log.info("...Inside IntegrationRunner...");
		BeforeTest bt = new BeforeTest();
		bt.beforeTest();
	}

	@AfterClass
	public static void tearDown() throws IOException {
		DriverFactory.getInstance().closeBrowser();
		System.clearProperty("featureFileName");
		RemoteConnection.exportCampaignPayloads(System.getProperty("set.BuildNumber") == null);
	}}