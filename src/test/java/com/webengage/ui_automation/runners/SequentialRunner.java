
package com.webengage.ui_automation.runners;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import com.webengage.ui_automation.driver.DriverFactory;
import com.webengage.ui_automation.initialize.RemoteConnection;
import com.webengage.ui_automation.utils.ExcelReader;
import com.webengage.ui_automation.utils.Log;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)

@CucumberOptions(//features = { "src/test/resources/features/tests/" },
		glue = {
		"com.webengage.ui_automation.stepDefinitions", "com.webengage.ui_automation.hooks" }, plugin = { "pretty",
				"json:target/cucumber-reports/SequentialCucumber.json",
				"junit:target/cucumber-reports/SequentialCucumber.xml" }, monochrome = true)
public class SequentialRunner {
	
	public static LinkedList<LinkedHashMap<String, String>> scenarioDetails = new LinkedList<>();

	@BeforeClass
	public static void initialize() throws IOException, InterruptedException, ParseException {
		Log.info("...Inside TestSequentialRunner...");
//		String env = "Prod US";
//		String credsUn = "aditya.dhanve@webengage.com";
//		String credsPw;
//		credsPw = env.contains("Staging") ? "@dmin123" : "@dmin123";
//		System.setProperty("set.LicenseCode", "null");
//		// System.setProperty("set.LicenseCode","~c2ab322b");
//		// System.setProperty("set.LicenseCode", "stg~~134106061");
//		System.setProperty("set.Environment", env);
//		System.setProperty("set.Namespace", "automation");
//		System.setProperty("set.CredentialsUN", credsUn);
//		System.setProperty("set.CredentialsPW", credsPw);
//		System.setProperty("set.Browser", "chrome");
		BeforeTest bt = new BeforeTest();
		bt.beforeTest();
	}

	@AfterClass
	public static void tearDown() throws IOException {
		DriverFactory.getInstance().closeBrowser();
		System.clearProperty("featureFileName");
		ExcelReader.initializeTestResultsSheet(scenarioDetails);
		RemoteConnection.exportCampaignPayloads(System.getProperty("set.BuildNumber") == null);
	}

}
