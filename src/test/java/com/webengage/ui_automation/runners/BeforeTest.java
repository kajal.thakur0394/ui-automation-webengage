package com.webengage.ui_automation.runners;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.CommonPage;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.webengage.ui_automation.elements.CommonPageElements;
import com.webengage.ui_automation.elements.LoginPageElements;
import com.webengage.ui_automation.initialize.RemoteConnection;
import com.webengage.ui_automation.pages.IntegrationsPage;
import com.webengage.ui_automation.pages.JourneysPage;
import com.webengage.ui_automation.pages.TestDataCreation;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.Log;
import com.webengage.ui_automation.utils.RestAssuredBuilder;
import com.webengage.ui_automation.utils.RuntimeUtility;
import com.webengage.ui_automation.utils.SeleniumExtendedUtility;
import com.webengage.ui_automation.utils.SetupUtility;

public class BeforeTest extends SeleniumExtendedUtility {
	public static String landingPageURL;

	public static String username = System.getProperty("set.CredentialsUN");
	public static String password = System.getProperty("set.CredentialsPW");
	public static String environment = System.getProperty("set.Environment");
	public static String namespace = System.getProperty("set.Namespace");
	public String sdkDomain_URL;
	SetupUtility setupUtility;
	TestDataCreation testDataCreation;

	public BeforeTest() throws IOException {
		setupUtility = new SetupUtility();
		setupUtility.loadConfigProp();
		testDataCreation = new TestDataCreation();
		try {
			writeBuildProperties();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeBuildProperties() throws IOException {
		Properties buildProps = new Properties();
		buildProps.put("browserName", ((RemoteWebDriver) driver).getCapabilities().getBrowserName().toUpperCase());
		buildProps.put("browserVersion", ((RemoteWebDriver) driver).getCapabilities().getBrowserVersion());
		buildProps.put("osName", System.getProperty("os.name"));
		buildProps.put("osVersion", System.getProperty("os.version"));
		buildProps.put("osArch", System.getProperty("os.arch"));
		buildProps.put("accountLicenseCode", "-");
		buildProps.put("accountName", "-");
		FileOutputStream outputStream = new FileOutputStream(System.getProperty("user.dir") + "/build.properties");
		buildProps.store(outputStream, "Build Properties for current build");
	}

	private void writeAccountDetailsinBuildProperies() throws IOException {
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "/build.properties");
		Properties buildProps = new Properties();
		buildProps.load(fis);
		buildProps.put("accountLicenseCode", System.getProperty("accountLicenseCode"));
		buildProps.put("accountName", System.getProperty("accountName"));
		FileOutputStream outputStream = new FileOutputStream(System.getProperty("user.dir") + "/build.properties");
		buildProps.store(outputStream, "Build Properties for current build");
	}

	public void login() throws InterruptedException, IOException {
		String Staging_URL = SetupUtility.configProp.getProperty("Staging_URL");
		if (environment.toLowerCase().contains("staging")) {
			openURL(SetupUtility.configProp.getProperty("Staging_Login_URL"));
			waitForElementToBeVisible(LoginPageElements.SIGN_IN.locate());
		} else if (environment.toLowerCase().contains("qa")) {
			driver.get(SetupUtility.configProp.getProperty("qa_Login_URL"));
			waitForElementToBeVisible(LoginPageElements.SIGN_IN.locate());
		} else {
			openURL(SetupUtility.configProp.getProperty("URL"));
			waitForElementToBeVisible(LoginPageElements.SIGN_IN.locate());
			if (environment.contains("IN"))
				click(LoginPageElements.REGION_INDIA.locate());
			else
				click(LoginPageElements.REGION_US.locate());
		}
		click(LoginPageElements.LOGIN_USERNAME.locate());
		sendKeys(LoginPageElements.LOGIN_USERNAME.locate(), username);
		sendKeys(LoginPageElements.LOGIN_PASSWORD.locate(), password);
		click(LoginPageElements.SIGN_IN.locate());
		Staging_URL = System.getProperty("set.LicenseCode").equals("null") ? Staging_URL
				: Staging_URL.replace("stg~~2024c06d", System.getProperty("set.LicenseCode"));
		switch (environment.toLowerCase()) {
		case "prod beta":
			openURL(getCurrentURL().replace("dashboard", "beta-dashboard"));
			break;
		case "prod unl":
			openURL(getCurrentURL().replace("dashboard", "dashboard.unl"));
			break;
		case "staging":
			openURL(Staging_URL.replace("namespace", namespace));
			break;
		case "staging dev":
			openURL(Staging_URL.replace("-namespace", ""));
			break;
		default:
			break;
		}
	}

	public void chooseProj() throws InterruptedException, IOException, ParseException {
		String licCode = System.getProperty("set.LicenseCode");
		String accountName = null;
		IntegrationsPage intPage = new IntegrationsPage();
		if (!licCode.equals("null")) {
			waitForElementToBeVisible(LoginPageElements.CHOOSE_PROJECT.locate());
			if (licCode.contains("new")) {
				createNewAccount(licCode.replace("new", ""));
			} else {
				openURL(getCurrentURL().replace(getCurrentURL().split("/")[4], licCode));
			}
			System.setProperty("accountName", fetchText(LoginPageElements.CHOOSE_PROJECT.locate()));
			intPage.openIntegrationPage();
			intPage.openRestApiPage();
			testDataCreation.fetchAPIKeys();
		} else {
			accountName = setAccountName(accountName);
			click(LoginPageElements.CHOOSE_PROJECT.locate());
			waitForElementToBeClickable(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, accountName));
			driver.findElement(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, accountName)).click();
			try {
				setupUtility.loadCredentials(accountName);
			} catch (Exception e) {
				Log.warn("Credentials for account you have chosen are not known.");
				intPage.openIntegrationPage();
				intPage.openRestApiPage();
				testDataCreation.fetchAPIKeys();
			}
			System.setProperty("accountName", accountName);
		}
		System.setProperty("accountLicenseCode", getCurrentURL().split("/")[4]);
		writeAccountDetailsinBuildProperies();
	}

	private String setAccountName(String accountName) {
		switch (environment.toLowerCase()) {
		case "staging dev":
		case "staging":
			accountName = "UIAutomationStaging";
			break;
		case "prod beta":
		case "prod us":
		case "prod unl":
			accountName = "UIAutomation";
			break;
		case "prod in":
			accountName = "UIAutomationIndia";
			break;
		default:
			break;
		}
		return accountName;

	}

	private void createNewAccount(String name) {
		click(LoginPageElements.CHOOSE_PROJECT.locate());
		click(dynamicXpathLocator(CommonPageElements.ANCHOR_TAG_GENERIC_TEXT, "Create New Project"));
		waitForElementToBeVisible(LoginPageElements.SAVE_BUTTON.locate());
		click(LoginPageElements.ACC_CATEGORY.locate());
		click(LoginPageElements.SELECT_CATEGORY.locate());
		click(LoginPageElements.ACC_TIMEZONE.locate());
		click(LoginPageElements.SELECT_TIMEZONE.locate());
		click(LoginPageElements.ACC_LANGUAGES.locate());
		click(LoginPageElements.SELECT_LANGUAGE.locate());
		sendKeys(LoginPageElements.ACC_NAME.locate(), name);
		click(LoginPageElements.SAVE_BUTTON.locate());
		click(LoginPageElements.USERS_TAB.locate());
	}

	public void beforeTest() throws InterruptedException, IOException, ParseException {
		initializeDataVariables();
		login();
		chooseProj();
		flushDirectoryContents();
		landingPageURL = getCurrentURL();
		Log.info("LPU: " + landingPageURL);
		String suite = System.getProperty("set.Suite");
		try {
			if (suite.equalsIgnoreCase(("WebSDKSuite"))) {
				configureSDK();
				editHTML();
			}
		} catch (Exception e) {
			Log.warn("WebSDK configuration skipped");
		}
	}

	private void initializeDataVariables() {
		APIUtility.initializeVariables();
		RuntimeUtility.initializeVariables();
		RestAssuredBuilder.initializeVariables();
		CampaignsPage.initializeVariables();
		JourneysPage.initializeVariables();
	}

	private void flushDirectoryContents() {
		Log.info("Deleting stuff from Expected, Runtime Payloads, and CampaignStats");
		File fileLocations[] = { new File(SetupUtility.runtimePayloadsDirectory),
				new File(SetupUtility.expectedPayloadsDirectory),
				new File(SetupUtility.runtimeCampaginStatsDirectory) };
		try {
			for (File file : fileLocations) {
				for (File dirFiles : file.listFiles()) {
					if (!dirFiles.getName().contains("gitkeep"))
						dirFiles.delete();
				}
			}
			RemoteConnection.readFilefromRemote("TestData", "testData.xlsx");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void configureSDK() {
		setSDKDomainURL();
		if (!fetchAttributeValue(LoginPageElements.DATAPLATFORM_LISTS.locate(), "class").contains("active"))
			click(LoginPageElements.DATAPLATFORM_LISTS.locate());
		click(LoginPageElements.INTEGRATIONS_PAGE.locate());
		click(LoginPageElements.CONFIGURE_SDK.locate());
		try {
			Thread.sleep(2000);
			Log.info("Existing configured SDK URL "
					+ fetchAttributeValue(LoginPageElements.DOMAIN_SDK.locate(), "value"));
			clearTextField(LoginPageElements.DOMAIN_SDK.locate());
			Thread.sleep(2000);
			sendKeys(LoginPageElements.DOMAIN_SDK.locate(),sdkDomain_URL);
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		click(LoginPageElements.SAVE_CONTINUE_BUTTON.locate());
	}

	private void editHTML() {
		String licenseCode = SetupUtility.licenseCode;
		Log.info("License Code :" + licenseCode);
		setDynamicSDKUrl();
		setSDKUser();
		Log.info("SDK User is : " +CommonPage.SDKUser);
		try {
			File file = new File(setFilePath());
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			StringBuilder sb = new StringBuilder("");
			while ((line = br.readLine()) != null) {
				if (line.contains("webengage.init")) {
					line = "webengage.init".concat("(\"" + licenseCode + "\");");
					Log.info("BeforeTest: LicenseCode:" + line);
				}
				if (line.contains("id='licenseCode'")) {
					line = "License Code : <p id='licenseCode'>".concat(licenseCode).concat("</p>");
				}
				sb.append(line).append("\n");
			}
			FileWriter fw = new FileWriter(file);
			fw.write(sb.toString());
			fw.close();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private void setDynamicSDKUrl() {
		CommonPage.SDKDynamicWebpage = SetupUtility.configProp.getProperty("SDKDynamicWebpage");
		switch(environment){
			case "Prod US":
				CommonPage.SDKDynamicWebpage =  CommonPage.SDKDynamicWebpage.replace("{environment}","us");
				break;
			case "Prod IN":
				CommonPage.SDKDynamicWebpage =  CommonPage.SDKDynamicWebpage.replace("{environment}","in");
				break;
			case "Staging":
				CommonPage.SDKDynamicWebpage = CommonPage.SDKDynamicWebpage.replace("{environment}","stg");
				break;
		}
	}

	private String setFilePath(){
		String path = null;
		switch (environment){
			case "Prod US":
				path = "C:\\Apache\\Apache24\\htdocs\\index-us.html";
				break;
			case "Prod IN":
				path = "C:\\Apache\\Apache24\\htdocs\\index-in.html";
				break;
			case "Staging":
				path = "C:\\Apache\\Apache24\\htdocs\\index-stg.html";
				break;
		}
		return path;
	}

	private void setSDKDomainURL() {
		sdkDomain_URL = SetupUtility.configProp.getProperty("sdkDomain_URL");
		switch (environment){
			case "Prod US":
				sdkDomain_URL = sdkDomain_URL.replace("{environment}","us");
				break;
			case "Prod IN":
				sdkDomain_URL = sdkDomain_URL.replace("{environment}","in");
				break;
			case "Staging":
				sdkDomain_URL = sdkDomain_URL.replace("{environment}","stg");
				break;
		}
	}

	private void setSDKUser() {
		CommonPage.SDKUser = SetupUtility.configProp.getProperty("SDK_User");
	}

}
