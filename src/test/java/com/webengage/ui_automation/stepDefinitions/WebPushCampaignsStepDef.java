package com.webengage.ui_automation.stepDefinitions;

import com.webengage.ui_automation.pages.WebPushCampaignsPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.io.IOException;

public class WebPushCampaignsStepDef {
    WebPushCampaignsPage webPushCampaignsPage = new WebPushCampaignsPage();
    @Given("I verify pre-execution checks for Web Push")
    public void i_verify_pre_execution_checks_for_Web_Push() throws IOException {
        webPushCampaignsPage.verifySDKStatus();
        webPushCampaignsPage.verifySDKDomain();
        webPushCampaignsPage.verifyWebPushStatus();
        webPushCampaignsPage.verifyWebPushToggle();
        webPushCampaignsPage.verifyUserReachability();

    }

    @Then("select Segment for Web Push")
    public void select_segment_for_web_push() throws IOException {
        webPushCampaignsPage.selectWebPushSDKSegment();
    }

}
