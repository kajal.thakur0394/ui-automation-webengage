package com.webengage.ui_automation.stepDefinitions;

import com.webengage.ui_automation.pages.AdditionalImplementationsPage;

import io.cucumber.java.en.Given;

public class AdditionalStepDefs {
	
	AdditionalImplementationsPage addImpl=new AdditionalImplementationsPage();

	@Given("I source the {string} stats for {string}")
	public void i_source_the_stats_for(String type, String channel) {
		addImpl.sourceStats(type, channel);
	}

}
