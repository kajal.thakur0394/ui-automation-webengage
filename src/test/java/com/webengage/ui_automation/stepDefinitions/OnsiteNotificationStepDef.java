package com.webengage.ui_automation.stepDefinitions;

import java.lang.reflect.InvocationTargetException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.webengage.ui_automation.elements.OnsiteNotificationPageElements;
import com.webengage.ui_automation.hooks.Hooks;
import com.webengage.ui_automation.pages.CampaignsPage;
import com.webengage.ui_automation.pages.OnsiteNotificationPage;
import com.webengage.ui_automation.utils.APIUtility;
import com.webengage.ui_automation.utils.ReflectionsUtility;
import com.webengage.ui_automation.utils.SetupUtility;
import io.cucumber.java.en.Then;

public class OnsiteNotificationStepDef {
	CampaignsPage campPage = new CampaignsPage();
	OnsiteNotificationPage onsitePage = new OnsiteNotificationPage();
	SetupUtility setupUtility = new SetupUtility();
	APIUtility apiUtility = new APIUtility();
	ReflectionsUtility refl = new ReflectionsUtility();

	@Then("I add {string} to the searchbox")
	public void i_add_to_the_searchbox(String campaignName) {
		onsitePage.searchBar(campaignName);
	}

	@Then("I verify that {string} and the {string} exist")
	public void i_verify_that_and_the_exist(String campaignName, String layoutType) {
		onsitePage.verifyCampaignNameandType(campaignName, layoutType);
	}

	@Then("create a {string} Notification")
	public void create_a_notification(String name) {
		onsitePage.createNotification(name);
	}

	@Then("select {string} template")
	public void select_template(String name) {
		onsitePage.selectOnsiteTemplate(name);
	}

	@Then("create a {string} Notification and select {string} template")
	public void create_a_notification_and_select_template(String noficationType, String templateType) {
		onsitePage.createNotification(noficationType);
		onsitePage.selectOnsiteTemplate(templateType);
	}

	@Then("use the personalization template {string} for {string} web-p")
	public void use_the_personalization_template_for_web_p(String fileName, String campaignType)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, InstantiationException, ClassNotFoundException, InterruptedException {
		JSONObject template = refl.fetchTemplate(Hooks.scenarioName, "PersonalizationTemplates", fileName);
		checkCampaignType(campaignType, template);
	}

	@Then("verify notification on a web page using template {string}")
	public void verify_notification_on_a_web_page_using_template(String fileName) {
		onsitePage.verifyOnsiteNotificationDetails(refl.getJSONTemplate());
	}

	@Then("delete the notification using template {string}")
	public void delete_the_notification_using_template(String fileName) {
		JSONObject template = refl.fetchTemplate(Hooks.scenarioName, "PersonalizationTemplates", fileName);
		onsitePage.deleteOnsiteNotification(template);
	}

	private void checkCampaignType(String campaignType, JSONObject template)
			throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, InstantiationException, ClassNotFoundException, InterruptedException {
		CampaignsPage.setCampaignType(campaignType);
		switch (campaignType) {
		case "On-site Surveys": {
			refl.readAndFillData((JSONArray) template.get("BasicDetails"));
			refl.readAndFillData((JSONArray) template.get("Questionnaire"));
			refl.readAndFillData((JSONArray) template.get("Targeting"));
			Thread.sleep(5000);
			refl.readAndFillData((JSONArray) template.get("Appearance"));
			refl.readAndFillData((JSONArray) template.get("Activate"));
			onsitePage.click(onsitePage.dynamicXpathLocator(OnsiteNotificationPageElements.LIST_PAGE, "surveys"));
			break;
		}
		case "On-site Notifications": {
			refl.readAndFillData((JSONArray) template.get("BasicDetails"));
			refl.readAndFillData((JSONArray) template.get("Targeting"));
			refl.readAndFillData((JSONArray) template.get("Activate"));
			onsitePage.click(onsitePage.dynamicXpathLocator(OnsiteNotificationPageElements.LIST_PAGE, "notifications"));
			break;
		}
		case "default": {
			break;
		}
		}

	}
}
