package com.webengage.ui_automation.stepDefinitions;

import java.util.List;

import com.webengage.ui_automation.pages.AnalyticsPage;
import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.utils.NotificationToastMessages;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;

public class AnalyticsStepDef {
	AnalyticsPage analyticsPage = new AnalyticsPage();
	CommonPage commonPage= new CommonPage();

	@Then("set event section filter as")
	public void set_event_section_filter_as(DataTable dataTable) {
		List<List<String>> filterOptions = dataTable.asLists(String.class);
		analyticsPage.eventFilterSelection(filterOptions);
	}

	@Then("verify following things get reflected in populated table")
	public void verify_following_things_get_reflected_in_populated_table(DataTable dataTable) {
		List<List<String>> cellValues = dataTable.asLists(String.class);
		analyticsPage.validateTableContent(cellValues);
	}

	@Then("fill the Path filter using below values")
	public void fill_the_path_filter_using_below_values(DataTable dataTable) {
		List<List<String>> pathFilters = dataTable.asLists(String.class);
		analyticsPage.pathFilterSelection(pathFilters);
	}

	@Then("validate whether event {string} gets reflected in Step {string}")
	public void validate_whether_event_gets_reflected_in_step(String eventName, String stepNumber) {
		analyticsPage.validateStepinHighChart(eventName, stepNumber);
	}
	
	@Then("create new Funnel with name as {string}")
	public void create_new_funnel_with_name_as(String funnelName) {
		commonPage.createIcon();
		analyticsPage.enterFunnelName(funnelName);
	}

	@Then("fill the Steps detail using below values")
	public void fill_the_steps_detail_using_below_values(DataTable dataTable) {
		List<List<String>> funnelStepFilters = dataTable.asLists(String.class);
		analyticsPage.addFunnelSteps(funnelStepFilters);
	}
	
	@Then("delete the funnel with name as {string}")
	public void delete_the_funnel_with_name_as(String funnelName) {
		commonPage.searchModuleNameHavingUnicode(funnelName);
		commonPage.deleteModule(funnelName);
		commonPage.verifyToastMessage(NotificationToastMessages.DELETE_FUNNEL.message());
	}
}
