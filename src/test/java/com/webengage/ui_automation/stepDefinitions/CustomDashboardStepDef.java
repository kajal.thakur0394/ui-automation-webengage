package com.webengage.ui_automation.stepDefinitions;

import com.webengage.ui_automation.pages.CommonPage;
import com.webengage.ui_automation.pages.CustomDashboardPage;
import io.cucumber.java.en.Then;

public class CustomDashboardStepDef {
	CustomDashboardPage custDashPage = new CustomDashboardPage();
	CommonPage comPage=new CommonPage();

	@Then("create new Dashboard with name as {string}")
	public void create_new_dashboard_with_name_as(String dashboardName) {
		comPage.createIcon();
		custDashPage.enterDashboardName(dashboardName);
	}


	@Then("select dashboard visibility as {string} and create a dashboard")
	public void select_dashboard_visibility_as_and_create_a_dashboard(String visibilityType) {
		custDashPage.selectDashboardVisibility(visibilityType);
		custDashPage.createDashboard();
	}
}
